﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic_qz_day04
{
    public class SplitKata
    {
        public void split(String[] arrayKata,int i)
        {
            if(arrayKata.Length-1 == i)
            {
                Console.WriteLine($"Kata {i + 1} = {arrayKata[i]}");
                Console.WriteLine($"Total Kata adalah {i+1}");
            }
            else
            {
                Console.WriteLine($"Kata {i+1} = {arrayKata[i]}");
                split(arrayKata, i + 1);
            }
        }
    }
}
