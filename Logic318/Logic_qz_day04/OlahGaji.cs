﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic_qz_day04
{
    public class OlahGaji
    {
        public int golongan;
        public float jamKerja;

        public int upahPerJam = 0;
        public int upahKerja = 0;
        public int upahLembur = 0;
        public OlahGaji(int golongan, float jamKerja) { 
            this.golongan = golongan;
            this.jamKerja = jamKerja;
        }

        public void hitung()
        {
            switch (golongan)
            {
                case 1:
                    upahPerJam = 2000;
                    break;
                case 2:
                    upahPerJam = 3000;
                    break;
                case 3:
                    upahPerJam = 3000;
                    break;
                case 4:
                    upahPerJam = 4000;
                    break;
                default:
                    upahPerJam = -1;
                    break;
            }
            if (jamKerja > 40)
            {
                float tamp = jamKerja - 40;
                upahKerja = upahPerJam * 40;
                upahLembur = (upahPerJam+(upahPerJam/2))*(int)Math.Round(tamp, MidpointRounding.AwayFromZero);
            }
            else
            {
                upahKerja = upahPerJam * (int)Math.Round(jamKerja, MidpointRounding.AwayFromZero);
            }
        }
        public void hasil()
        {
            Console.WriteLine($"Upah \t \t {upahKerja}");
            Console.WriteLine($"Lembur \t \t {upahLembur}");
            Console.WriteLine($"Total \t \t {upahKerja+upahLembur}");
        }
    }
}
