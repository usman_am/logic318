﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic08
{
    internal class SortingTheAlphabets
    {
        public SortingTheAlphabets()
        { //A 65  a 97
            Console.WriteLine("== Sorting The Alphabets ==");
            Console.Write("text : ");
            string str = Console.ReadLine();
            string[] strArray = str.Select(x => x.ToString()).ToArray();
            //char[] text = Console.ReadLine().ToCharArray();
            /*text = Sorting.ArraySort(text);
            foreach (char item in text)
            {
                Console.Write($"{item} ");
            }*/
            strArray = Sorting.ArraySort(strArray);
            foreach (String item in strArray)
            {
                Console.Write($"{item} ");
            }

        }
    }
}
