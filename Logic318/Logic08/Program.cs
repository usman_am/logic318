﻿namespace Logic08
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Program Pertemuan 8");
                Console.WriteLine("1.Insertion Sort");
                Console.WriteLine("7.Median");
                Console.WriteLine("8.Sorting The Alphabets");
                Console.Write("Pilih : ");
                int pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 0: break;
                    case 1:
                        InsertionSort hasil01 = new InsertionSort();
                        break;
                    case 7:
                        Median hasil07 = new Median();
                        break;
                    case 8:
                        SortingTheAlphabets hasil08 = new SortingTheAlphabets();
                        break;
                    default: Console.WriteLine("Pilih 1,7,8"); break;
                }

                if (pilih == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin megulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
        }
    }
}