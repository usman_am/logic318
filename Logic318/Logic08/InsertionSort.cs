﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic08
{
    internal class InsertionSort
    {
        public InsertionSort() {
            Console.WriteLine("== Isertion Sort ==");
            Console.Write("number : ");
            String[] number = Console.ReadLine().Split(" ");
            int[] arrayi = Array.ConvertAll<String, int>(number, int.Parse);

            arrayi = Sorting.ArraySort(arrayi);
            foreach (int item in arrayi)
            {
                Console.Write($"{item} ");
            }
        }
    }
}
