﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic08
{
    internal class Median
    {
        public Median() {
            //0 1 2 4 6 5 3
            //3
            Console.WriteLine("== Median ==");
            Console.Write("number : ");
            String[] number = Console.ReadLine().Split(" ");
            int[] arrayi = Array.ConvertAll<String, int>(number, int.Parse);

            arrayi = Sorting.ArraySort(arrayi);

            foreach (int item in arrayi)
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();

            if (arrayi.Length%2==0 ) {
                float n1 = arrayi[arrayi.Length / 2];
                float n2 = arrayi[arrayi.Length / 2 - 1];
                Console.Write($"{(n1+n2)/2}");
            }
            else
            {
                Console.WriteLine($"{arrayi[arrayi.Length / 2]} ");
            }
            
        }
    }
}
