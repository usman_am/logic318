﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class Sorting
    {
        public static int[] ArraySort(int[] arr)
        {
            for(int i=0; i<arr.Length; i++)
            {
                for(int j=0; j<i; j++)
                {
                    if (arr[i] < arr[j])
                    {
                        int nTamp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = nTamp;
                    }
                }
            }
            return arr;
        }

        public static char[] ArraySort(char[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    if (arr[i] < arr[j])
                    {
                        char nTamp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = nTamp;
                    }
                }
            }
            return arr;
        }

        public static String[] ArraySort(string[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    if (arr[i].CompareTo(arr[j])<0)
                    {
                        String nTamp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = nTamp;
                    }
                }
            }
            return arr;
        }
    }
}
