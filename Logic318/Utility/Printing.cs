﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class Printing
    {
        public static void Array1Dim(String[] arr)
        {
            for(int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i]+" ");
            }
        }

        public static void Array2Dim(String[,] arr)
        {
            for (int i = 0;i < arr.GetLength(0); i++)
            {
                for(int j=0;j<arr.GetLength(1); j++)
                {
                    Console.Write($"{arr[i, j]}\t");
                }
                Console.WriteLine();
            }
        }

       
    }
}
