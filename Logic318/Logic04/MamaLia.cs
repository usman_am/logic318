﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic04_Abstrak
{
    public class MamaLia
    {
        public virtual void pindah() //tambahkan virtual
        {
            Console.WriteLine("Berlari...");
        }
    }

    public class Kucing : MamaLia { }

    public class paus : MamaLia {
        public override void pindah()
        {
            Console.WriteLine("Berenang...");
        }
    }
}
