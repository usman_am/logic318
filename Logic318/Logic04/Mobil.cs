﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic04_Abstrak
{
    public class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        public string nama;
        public string platno;

        public Mobil(string _platno) { 
            platno = _platno;
        }

        public Mobil() { }

        public string getPlatNo()
        {
            return platno;
        }

        public void utama()
        {
            Console.WriteLine($"Nama = {nama}");
            Console.WriteLine($"Bensin = {bensin}");
            Console.WriteLine($"posisi = {posisi}");
            Console.WriteLine($"kecepatan = {kecepatan}");

        }

            public void percepat()
        {
            this.kecepatan += 10;
            this.bensin -= 5;
        }

        public void maju()
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }

        public void isiBensin(double bensin)
        {
            this.bensin += bensin;
        }
    }
}
