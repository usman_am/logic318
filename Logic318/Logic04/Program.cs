﻿using Logic04;
using Logic04_Abstrak;
//objekclass();
//kelasabstrak();
//costructor();
//enkapsulation();
//inheritance();
//overridingploymorphis();
rekursiffungsi();
Console.ReadKey();

static void rekursiffungsi() //fungsi yang memanggil dirinya sendiri
{
    Console.Write("masukkan nilai : ");
    int input = int.Parse(Console.ReadLine());
    perulangan(input);

}

static int perulangan(int input)
{
    if(input == 1)
    {
        Console.WriteLine($"{input}");
        return input;
    }
    Console.WriteLine($"{input}");
    return perulangan(input - 1);
}

static void overridingploymorphis()
{
    Kucing kucing = new Kucing();
    paus paus = new paus();
    kucing.pindah();
    paus.pindah();
}

static void inheritance()
{
    TipeMobil tm = new TipeMobil();
    tm.civic();
}

static void enkapsulation()
{
    PersegiPanjang persegi = new PersegiPanjang() { lebar = 10, panjang=2 };
    persegi.TampilkanData();
}

static void costructor()
{
    Console.WriteLine();
    Mobil mobil = new Mobil("BG 0000 FE");
    string platno = mobil.getPlatNo();
    Console.WriteLine(platno);
}

static void objekclass()
{
    Mobil mobil = new Mobil("") { nama = "ferrari", kecepatan = 0, bensin = 10, posisi = 0 };
    //Mobil mobil2 = new Mobil();
    //mobil2.nama = "nissan";
    //mobil2.kecepatan = 0;
    //mobil2.bensin = 10;
    //mobil2.posisi = 0;

    mobil.percepat();
    mobil.maju();
    mobil.isiBensin(20);
    mobil.utama();
    
}

    static void kelasabstrak() 
{
    Console.WriteLine("-- Abstact Class --");
    Console.Write("Masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai y : ");
    int y = int.Parse(Console.ReadLine());

    testTurunan testing = new testTurunan();
    int jumlah = testing.jumlah(x, y);
    int kurang = testing.kurang(x, y);

    Console.WriteLine($"Hasil {x}+{y} = {jumlah}");
    Console.WriteLine($"Hasil {x}-{y} = {kurang}");
}
