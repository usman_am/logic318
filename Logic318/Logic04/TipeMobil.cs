﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic04_Abstrak
{
    public class TipeMobil : Mobil
    {
        public void civic()
        {
            nama = "Suzuki R3";
            platno = "B 112233 FE";
            bensin = 10;

            utama();
        }
    }
}
