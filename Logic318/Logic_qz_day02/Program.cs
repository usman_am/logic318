﻿
using System.ComponentModel.DataAnnotations;
using System.Reflection.PortableExecutable;

//luas_dan_keliling_lingkaran();    //soal 1
//luas_dan_keliling_persegi();      //soal 2
//cari_hasil_modulus();             //soal 3
//rangkai_puntung_rokok();          //soal 4
//grade_nilai();                    //soal 5
//ganjil_genap();                   //soal 6
quiz_per02();
Console.ReadKey();  

static void quiz_per02()
{
    while (true)
    {
        Console.WriteLine("");
        Console.WriteLine("-- Program Quiz Pertemua 02 bootcamp .net --");
        Console.WriteLine("1. Menghitung luas & keliling lingkaran");
        Console.WriteLine("2. Menghitung luas & keliling persegi");
        Console.WriteLine("3. Cari hasil modulus");
        Console.WriteLine("4. Pemulung merangkai puntung rokok");
        Console.WriteLine("5. Tentukan grade nilai");
        Console.WriteLine("6. Cek ganjil / genap");
        Console.WriteLine("0. keluar");
        Console.Write("pilih salah satu? : ");
        int nilai = int.Parse(Console.ReadLine());
        if(nilai == 1)
        {
            Console.WriteLine("");
            Console.WriteLine("-- Program meghitung Luas dan Keliling lingkaran --");
            Console.Write("masukkan jari-jari : ");
            float jari = float.Parse(Console.ReadLine());
            double vr = 22.0 / 7.0;
            double k = (2 * vr * jari);
            double l = (vr * jari * jari);
            Console.WriteLine($"Luas jari-jari : {(int)Math.Round(l, 0)}cm^2 dan Keliling : {(int)Math.Round(k, 0)}cm");
            Console.Write("tekan enter untuk melanjutkan...");
            Console.ReadLine();
        }else if(nilai == 2)
        {
            Console.WriteLine("");
            Console.WriteLine("-- Program meghitung Luas dan Keliling persegi --");
            Console.Write("masukkan sisi : ");
            int sisi = int.Parse(Console.ReadLine());
            int l = (sisi * sisi);
            int k = (4 * sisi);
            Console.WriteLine($"Persegi memiliki Luas : {l} dan Keliling : {k}");
            Console.Write("tekan enter untuk melanjutkan...");
            Console.ReadLine();
        }
        else if(nilai == 3)
        {
            Console.WriteLine("");
            Console.WriteLine("-- Program cari hasil modulus --");
            Console.Write("masukkan angka : ");
            int angka = int.Parse(Console.ReadLine());
            Console.Write("masukkan pembagi : ");
            int pembagi = int.Parse(Console.ReadLine());

            int h = angka % pembagi;
            if (h == 0)
            {
                Console.WriteLine($"angka {angka} % {pembagi} adalah 0");
            }
            else
            {
                Console.WriteLine($"angka {angka} % {pembagi} bukan 0 melainkan {h}");
            }
            Console.Write("tekan enter untuk melanjutkan...");
            Console.ReadLine();
        }
        else if(nilai == 4)
        {
            Console.WriteLine("");
            Console.WriteLine("-- Program rangkai puntung rokok --");
            Console.Write("masukkan jumlah : ");
            int angka = int.Parse(Console.ReadLine());

            int pj = 0;
            while (angka >= 8)
            {
                if (angka >= 20)
                {
                    pj += 2;
                    angka -= 16;
                }
                else
                {
                    pj += 1;
                    angka -= 8;
                }
            }
            Console.WriteLine($"Pemulung merangkai {pj} batang rokok dengan sisa {angka} puntung rokok");
            Console.WriteLine($"Pemulung mendapat penghasilan sebesar Rp.{pj * 500},-");
            Console.Write("tekan enter untuk melanjutkan...");
            Console.ReadLine();
        }
        else if(nilai == 5)
        {
            Console.WriteLine("");
            Console.WriteLine("-- Program grade nilai --");
            Console.Write("masukkan nilai : ");
            int nilain = int.Parse(Console.ReadLine());
            if (nilain >= 80)
            {
                Console.WriteLine("Kamu dapat A");
            }
            else if (nilain >= 60 && nilain < 80)
            {
                Console.WriteLine("kamu dapat B");
            }
            else
            {
                Console.WriteLine("Kamu dapat C");
            }
            Console.Write("tekan enter untuk melanjutkan...");
            Console.ReadLine();
        }
        else if(nilai == 6)
        {
            Console.WriteLine("");
            Console.WriteLine("-- Program ganjil genap --");
            Console.Write("masukkan angka : ");
            int nilais = int.Parse(Console.ReadLine());
            if (nilais % 2 == 0)
            {
                Console.WriteLine($"angka {nilais} adalah genap");
            }
            else
            {
                Console.WriteLine($"angka {nilais} adalah ganjil");
            }
            Console.Write("tekan enter untuk melanjutkan...");
            Console.ReadLine();
        }
        else if(nilai == 0)
        {
            break;
        }
        else { 
            Console.WriteLine("masukkan angka yang sesuai");
            Console.Write("tekan enter untuk melanjutkan...");
            Console.ReadLine();
        }
    }
}

static void ganjil_genap()
{
    Console.WriteLine("-- Program ganjil genap --");
    Console.Write("masukkan angka : ");
    int nilai = int.Parse(Console.ReadLine());
    if (nilai % 2 == 0)
    {
        Console.WriteLine($"angka {nilai} adalah genap");
    }
    else
    {
        Console.WriteLine($"angka {nilai} adalah ganjil");
    }
}

static void grade_nilai()
{
    Console.WriteLine("-- Program grade nilai --");
    Console.Write("masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    if(nilai >= 80)
    {
        Console.WriteLine("Kamu dapat A");
    }else if(nilai >= 60 && nilai < 80)
    {
        Console.WriteLine("kamu dapat B");
    }
    else
    {
        Console.WriteLine("Kamu dapat C");
    }
}

static void rangkai_puntung_rokok()
{
    Console.WriteLine("-- Program rangkai puntung rokok --");
    Console.Write("masukkan jumlah : ");
    int angka = int.Parse(Console.ReadLine());

    int pj = 0;
    while(angka >= 8)
    {
        if(angka >= 20)
        {
            pj += 2;
            angka -= 16;
        }
        else
        {
            pj += 1;
            angka -= 8;
        }
    }
    Console.WriteLine($"Pemulung merangkai {pj} batang rokok dengan sisa {angka} puntung rokok");
    Console.WriteLine($"Pemulung mendapat penghasilan sebesar Rp.{pj*500},-");
}

static void cari_hasil_modulus()
{
    Console.WriteLine("-- Program cari hasil modulus --");
    Console.Write("masukkan angka : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("masukkan pembagi : ");
    int pembagi = int.Parse(Console.ReadLine());

    int h = angka % pembagi;
    if(h == 0)
    {
        Console.WriteLine($"angka {angka} % {pembagi} adalah 0");
    }
    else
    {
        Console.WriteLine($"angka {angka} % {pembagi} bukan 0 melainkan {h}");
    }
}

static void luas_dan_keliling_persegi()
{
    Console.WriteLine("-- Program meghitung Luas dan Keliling persegi --");
    Console.Write("masukkan sisi : ");
    int sisi = int.Parse(Console.ReadLine());
    int l = (sisi*sisi);
    int k = (4*sisi);
    Console.WriteLine($"Persegi memiliki Luas : {l} dan Keliling : {k}");
}

static void luas_dan_keliling_lingkaran()
{
    Console.WriteLine("-- Program meghitung Luas dan Keliling lingkaran --");
    Console.Write("masukkan jari-jari : ");
    float jari = float.Parse(Console.ReadLine());
    double vr = 22.0 / 7.0;
    double k = (2* vr * jari);
    double l = (vr * jari * jari);
    Console.WriteLine($"Luas jari-jari : {(int)Math.Round(l, 0)}cm^2 dan Keliling : {(int)Math.Round(k, 0)}cm");
}
