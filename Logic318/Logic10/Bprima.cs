﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class Bprima
    {
        public Bprima()
        {
            Console.WriteLine("== Prima ==");
            Console.Write("Deret angka : ");
            int nilai = int.Parse(Console.ReadLine());
            List<int> list = new List<int>();
            int p1 = 1;
            for (int i = 0; i < nilai; i++)
            {
                while (true)
                {
                    p1++;
                    int counter = 0;
                    for (int j = 1; j <= p1; j++)
                    {
                        if (p1 % j == 0)
                        {
                            counter++;
                        }
                    }
                    if (counter == 2)
                    {
                        list.Add(p1);
                        break;
                    }
                }
            }
            foreach (var item in list)
            {
                Console.Write(item + " ");
            }
        }
    }
}
