﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic10
{
    internal class Chessboard2
    {
        public Chessboard2()
        {

            Console.WriteLine("== Chess Board ==");
            Console.Write("Queen White : ");
            String qWhite = Console.ReadLine();
            Console.Write("Queen Black : ");
            String qBlack = Console.ReadLine();
            String[,] cBoard = new String[4,4];
            bool hasil = false;
            string rowMap = "4321";
            string colMap = "ABCD";

            for(int i = 0; i < cBoard.GetLength(0); i++)
            {
                for(int j = 0; j < cBoard.GetLength(1); j++)
                {
                    if (rowMap.IndexOf(qWhite[1])==i || colMap.IndexOf(qWhite[0])==j ||
                        rowMap.IndexOf(qWhite[1])+j == colMap.IndexOf(qWhite[0])+i ||
                         rowMap.IndexOf(qWhite[1]) + colMap.IndexOf(qWhite[0]) == i + j)
                        cBoard[i, j] = "X";
                    else
                        cBoard[i, j] = ".";
                }
            }
            if (cBoard[rowMap.IndexOf(qBlack[1]), colMap.IndexOf(qBlack[0])] == "X") { Console.WriteLine("YES"); } else { Console.WriteLine("NO"); }
            cBoard[rowMap.IndexOf(qBlack[1]), colMap.IndexOf(qBlack[0])] = "QB";
            cBoard[rowMap.IndexOf(qWhite[1]), colMap.IndexOf(qWhite[0])] = "QW";
            Printing.Array2Dim(cBoard);
            
            
            
        }
    }
}
