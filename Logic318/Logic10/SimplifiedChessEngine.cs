﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class SimplifiedChessEngine
    {
        public SimplifiedChessEngine()
        {
            Console.WriteLine("== Simplified Chess Engine ==");
            Console.Write("input : ");
            String[] iTamp = Console.ReadLine().Split(" ");
            String[,] wChess = new String[int.Parse(iTamp[0]),3];
            for(int i=0; i < int.Parse(iTamp[0]); i++)
            {
                Console.Write("White Position : ");
                String[] array1 = Console.ReadLine().Split(" ");
                for(int j=0; j < array1.Length; j++)
                {
                    wChess[i,j] = array1[j];
                }
            }

            string[,] bChess = new string[int.Parse(iTamp[1]), 3];
            for (int i = 0; i < int.Parse(iTamp[1]); i++)
            {
                Console.Write("Black Position : ");
                String[] array1 = Console.ReadLine().Split(" ");
                for (int j = 0; j < array1.Length; j++)
                {
                    bChess[i, j] = array1[j];
                }
            }
            String baris = "1234"; String kolom = "abcd";
            for (int i = 0; i < int.Parse(iTamp[2]); i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        if (baris.IndexOf(wChess[1,i])==i){ Console.Write(" X "); }
                        else { Console.Write(" * "); }
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
