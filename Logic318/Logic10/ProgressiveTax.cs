﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class ProgressiveTax
    {
        public ProgressiveTax()
        {
            Console.WriteLine("== Gross Up Tax ==");
            Console.WriteLine("Employee salary cut by progressive when they get more than 25000");
            Console.WriteLine("Instruction :");
            Console.WriteLine("Range            Tax");
            Console.WriteLine("25000-50000      5%");
            Console.WriteLine("50000-100000     10%");
            Console.WriteLine("100000-200000    15%");
            Console.WriteLine(">200000          25%");
            Console.Write("Your Salary : ");
            int salary = int.Parse(Console.ReadLine());
            int pajak = 0;
            float hasil = hitung(salary, pajak);
            
            Console.WriteLine(hasil);
            Console.WriteLine($"Income          {salary}");
            Console.WriteLine($"Tax Allowance   {hasil}  +");
            Console.WriteLine($"Total Income    {salary+hasil}");
            Console.WriteLine($"Free Tax        25000  -");
            Console.WriteLine($"Taxable-Income  {salary+hasil-25000}");
            Console.WriteLine($"Tax Payable     {hasil}");
        }

        public static float hitung(int salary,int pajak)
        {
            int slrasli = salary;
            float pjk = 0;
            int sal = salary+pajak;
            if (sal > 25000) { sal -= 25000; }
            while (sal > 0)
            {
                if (sal > 200000) { pjk += (sal - 200000) * 0.25f; sal = 200000; }
                else if (sal > 100000) { pjk += (sal - 100000) * 0.15f; sal = 100000; }
                else if (sal > 50000) { pjk += (sal - 50000) * 0.10f; sal = 50000; }
                else if (sal > 0) { pjk += (sal) * 0.05f; sal = 0; }
                Console.WriteLine(sal);
            }
            int ppjk = (int)Math.Floor(pjk);
            Console.WriteLine($"{ppjk}, {pajak}");
            //return ppjk;
            if (pajak == ppjk) { return ppjk; }
            else { return hitung(slrasli, ppjk); }
        }
    }
}
