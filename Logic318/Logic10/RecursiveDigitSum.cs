﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class RecursiveDigitSum
    {
        public RecursiveDigitSum()
        {
            Console.WriteLine("== Recursive Digit Sum ==");
            Console.Write("Number & counter : ");
            String[] iTamp = Console.ReadLine().Split(" ");
            String nTamp = "";
            for (int i=0; i < int.Parse(iTamp[1]); i++)
            {
                nTamp += iTamp[0];
            }
            iTamp[0] = nTamp;
            string hasil = Hitung(iTamp[0]);
            Console.WriteLine(hasil);
        }
        public static string Hitung(string nilai)
        {
            String[] array1 = nilai.Select(x => x.ToString()).ToArray();
            int[] arraya = Array.ConvertAll<String, int>(array1, int.Parse);
            int nTamp = 0;
            for(int i=0; i < arraya.Length; i++)
            {
                nTamp += arraya[i];
            }
            if (nilai.Length > 1) { return Hitung(nTamp.ToString()); }
            else { return nTamp.ToString(); }
        }
    }
}
