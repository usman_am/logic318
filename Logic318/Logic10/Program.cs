﻿namespace Logic10
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Program Pertemuan 10");
                Console.WriteLine("1.Recursive Digit Sum");
                Console.WriteLine("2.Simplified Chess Engine");
                Console.WriteLine("3.ChessBoard");
                Console.WriteLine("4.Cek Prima");
                Console.WriteLine("5.Password Cracker");
                Console.WriteLine("6.PRogressive Tax");
                Console.WriteLine("0.Main Menu");
                Console.Write("Pilih : ");
                int pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 0: break;
                    case 1: RecursiveDigitSum soal01 = new RecursiveDigitSum(); break;
                    case 2: SimplifiedChessEngine soal02 = new SimplifiedChessEngine(); break;
                    case 3: Chessboard2 soal03 = new Chessboard2(); break;
                    case 4: Bprima soal04 = new Bprima(); break;
                    case 5: PasswordCracker soal05 = new PasswordCracker(); break;
                    case 6: ProgressiveTax soal06 = new ProgressiveTax(); break;
                    default: Console.WriteLine("Pilih 1-8"); break;
                }

                if (pilih == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin megulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
        }
    }
}