﻿using System;
//gradeNilaiRata2();
//bodyMassIndex();
//tunjanganGaji();
//babyboomer();
//grabFood();
//pointBeliPulsa();
//setGrade();
marketSope();

Console.ReadKey();

static void gradeNilaiRata2()
{
    Console.WriteLine("-- Program Menentukan Grade Nilai Rata2 --");
    Console.Write("Masukkan nilai MTK : ");
    int nilaiMtk = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai Fisika : ");
    int nilaiFsk = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai Kimia : ");
    int nilaiKma = int.Parse(Console.ReadLine());
    int hasilNilaiMhs = (nilaiMtk+nilaiFsk+nilaiKma)/3;
    if (hasilNilaiMhs >= 50) { Console.WriteLine("Selamat kamu berhasil"); }
    else { Console.WriteLine("Maaf kamu gagal"); }
    
}

static void bodyMassIndex()
{
    Console.WriteLine("-- Program Body Mass Index --");
    Console.Write("Masukkan berat badan anda (kg) : ");
    int berat = int.Parse(Console.ReadLine());
    Console.Write("Masukkan tinggi badan anda (cm) : ");
    float tinggi = float.Parse(Console.ReadLine());
    float tTinggi = (tinggi / 100) * (tinggi / 100);
    float hasil = (berat / tTinggi);
    String bmi = "";
    if (hasil < 18.5) { bmi = "terlalu kurus"; }
    else if(hasil < 25) { bmi = "langsing/sehat"; }
    else { bmi = "tergolong gemuk"; }

    Console.WriteLine("Nilai BMI anda adalah {0}",Math.Round((berat/tTinggi),4));
    Console.WriteLine("Anda termasuk berbadan " + bmi);
}

static void tunjanganGaji()
{
    Console.WriteLine("-- Program Hitung Total Gaji --");
    Console.Write("Nama : ");
    String nama2 = Console.ReadLine();
    Console.Write("Tunjangan : ");
    int tunjangan = int.Parse(Console.ReadLine());
    Console.Write("Gapok : ");
    int gapok = int.Parse(Console.ReadLine());
    Console.Write("Banyak Bulan : ");
    int bulan = int.Parse(Console.ReadLine());

    int pajak = 0;
    if (gapok + tunjangan <= 5000000) { pajak = 5; }
    else if (gapok + tunjangan <= 10000000) { pajak = 10; }
    else if (gapok + tunjangan > 10000000) { pajak = 15; }
    int topajak = ((gapok + tunjangan) / 100) * pajak;
    int tobpjs = ((gapok + tunjangan) / 100) * 3;

    Console.WriteLine($"Karyawan atas nama {nama2} slip gaji sebagai berikut : ");
    Console.WriteLine($"Pajak : \t \t Rp.{topajak},- ");
    Console.WriteLine($"Bpjs : \t \t \t Rp.{tobpjs},- ");
    Console.WriteLine($"Gaji/bln : \t \t Rp.{(gapok + tunjangan) - (topajak+tobpjs)},- ");
    Console.WriteLine($"Total gaji/banyak bulan  Rp.{((gapok + tunjangan) - (topajak + tobpjs))*bulan},- ");
}

static void babyboomer()
{
    Console.WriteLine("-- Program Cek Kamu Generasi Apa --");
    Console.Write("Masukkan nama anda : ");
    String nama = Console.ReadLine();
    Console.Write("Tahun berapa anda lahir? : ");
    int kelahiran = int.Parse(Console.ReadLine());

    String gen = "";
    if (kelahiran < 1965) { gen = "Boomer"; }
    else if (kelahiran < 1980) { gen = "X"; }
    else if (kelahiran < 1995) { gen = "Y"; }
    else { gen = "Z"; }

    Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong generasi {gen}");
}

static void marketSope()
{
    while (true)
    {
        Console.WriteLine("-- Program Belanja di Sope --");
        Console.Write("Belanja : ");
        int uang2 = int.Parse(Console.ReadLine());
        Console.Write("Ongkos kirim : ");
        int ongkos2 = int.Parse(Console.ReadLine());
        Console.Write("Pilih voucher : ");
        int voucher2 = int.Parse(Console.ReadLine());
        Console.WriteLine("-------------------------");

        int diskonOngkir = 0;
        int diskonBelanja = 0;
        int vc = 0;
        switch (voucher2)
        {
            case 1:
                if (uang2 >= 30000) { diskonOngkir = 5000; diskonBelanja = 5000; }
                else { vc = 1; }
                break;
            case 2:
                if (uang2 >= 50000) { diskonOngkir = 10000; diskonBelanja = 10000; }
                else { vc = 1; }
                break;
            case 3:
                if (uang2 >= 100000) { diskonOngkir = 20000; diskonBelanja = 10000; }
                else { vc = 1; }
                break;
        }

        if (vc == 1) { Console.WriteLine($"Voucher tidak dapat digunakan karena pembayaran tidak mencukupi min belanja"); }
        else
        {
            int ongkirtotal = ongkos2 - diskonOngkir;
            if(ongkirtotal < 0) { ongkirtotal = 0; }
            Console.WriteLine($"Belanja : \t \t {uang2} ");
            Console.WriteLine($"Ongkos Kirim : \t \t {ongkos2} ");
            Console.WriteLine($"Diskon Ongkir : \t {diskonOngkir} ");
            Console.WriteLine($"Ongkir Total : \t \t {ongkirtotal} ");
            Console.WriteLine($"Diskon Belanja : \t {diskonBelanja} ");
            Console.WriteLine($"Total Belanja : \t \t {ongkirtotal + uang2 - diskonBelanja} ");
        }
        break;
    }

    
    
}

static void grabFood()
{
    try
    {
        Console.WriteLine("-- Program Pesan Grab Food --");
        Console.Write("Belanja : ");
        int uang = int.Parse(Console.ReadLine());
        Console.Write("Jarak per KM : ");
        double jarak = double.Parse(Console.ReadLine());
        Console.Write("Masukka Promo = ");
        String promo = Console.ReadLine().ToUpper();
        Console.WriteLine("-------------------------");
        if (uang < 0 || jarak < 0)
        {
            Console.WriteLine("Masukkan nilai belanja atau jarak yang sesuai");
        }
        else if (uang == 0)
        {
            Console.WriteLine("masukkan nilai belanja yang sesuai");
        }
        else
        {
            int diskon = 40;
            int ongkir = 5000;
            int totalDisc = 0;
            int todi = 0;
            if (promo == "JKTOVO")
            {
                if (uang >= 30000) { totalDisc = (uang / 100) * diskon; }
                else { todi = -1; }
            }

            while (jarak > 5) { ongkir += 1000; jarak--; }

            int total = uang - totalDisc + ongkir;

            if (todi < 0)
            {
                Console.WriteLine($"Anda tidak mencapai minimal belanja untuk menggunakan voucher {promo} ");
            }

            Console.WriteLine($"Belanja : \t {uang} ");
            Console.WriteLine($"Diskon 40% : \t {totalDisc} ");
            Console.WriteLine($"Ongkir : \t {ongkir} ");
            Console.WriteLine($"Total Belanja \t {total} ");
        }
    }
    catch(Exception e)
    {
        Console.WriteLine("Masukkan nilai belanja atau jarak yang sesuai");
    }
   

    
    
    
}

static void pointBeliPulsa()
{
    Console.WriteLine("-- Program Point Pembelian Pulsa --");
    Console.Write("Masukkan jumlah pulsa : ");
    int pulsa = int.Parse(Console.ReadLine());
    int poin = 0;
    if (pulsa < 10000) { poin = 0; }
    else if (pulsa < 25000) { poin = 80; }
    else if (pulsa < 50000) { poin = 200; }
    else if (pulsa < 100000) { poin = 400; }
    else { poin = 800; }
    Console.WriteLine("Anda mendapatkan Poin {0}", poin);
}

static void setGrade()
{
    Console.WriteLine("-- Program Menentukan Grade Mahasiswa --");
    Console.Write("Masukkan nilai : ");
    int nilaiMhs = int.Parse(Console.ReadLine());
    String hasilNilaiMhs = "";
    if (nilaiMhs >= 90 && nilaiMhs <= 100) { hasilNilaiMhs = "A"; }
    else if (nilaiMhs >= 70 && nilaiMhs < 90) { hasilNilaiMhs = "B"; }
    else if (nilaiMhs >= 50 &&  nilaiMhs < 70) { hasilNilaiMhs = "C"; }
    else { hasilNilaiMhs = "D"; }
    Console.WriteLine("Anda mendapatkan Grade {0}",hasilNilaiMhs);
}
