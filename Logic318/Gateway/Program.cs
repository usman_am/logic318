﻿namespace Gateway;

public class Program
{
    static void Main(string[] args)//mandatory dari project
    {
        while (true)
        {
            Console.WriteLine("== Welcome to batch 318 ==");
            Console.WriteLine("5. 1 & 2 Dimension Array");
            Console.WriteLine("6. Warmup");
            Console.WriteLine("7. Strings");
            Console.WriteLine("8. Sorting");
            Console.WriteLine("9. Search");
            Console.WriteLine("10. Recursion");
            Console.WriteLine("0. exit");
            Console.Write("Pilihan anda : ");
            int hari = int.Parse(Console.ReadLine());
            switch (hari)
            {
                case 5:
                    Console.WriteLine("1.Array 1 Dimensi");
                    Console.WriteLine("2.Array 2 Dimensi");
                    Console.WriteLine("0.Kembali");
                    Console.Write("Pilih : ");
                    int hari5 = int.Parse(Console.ReadLine());
                    switch (hari5)
                    {
                        case 0: break;
                        case 1: Logic05.Program program05 = new Logic05.Program(); break;
                        case 2: Logic05_part2.Program program05b = new Logic05_part2.Program(); break;
                        default: Console.WriteLine("Pilih angka yang benar..."); continue;
                    }
                    break;
                case 6: Logic06.Program program06 = new Logic06.Program(); break;
                case 7: Logic07.Program program07 = new Logic07.Program(); break;
                case 8: Logic08.Program program08 = new Logic08.Program(); break;
                case 9: Logic09.Program program09 = new Logic09.Program(); break;
                case 10: Logic10.Program program10 = new Logic10.Program(); break;
                default: Console.Write("Masukkan angka 5-6"); break;
            }
            Console.WriteLine();
            if (hari!=0) { Console.WriteLine(); continue; }
            else { Console.WriteLine(); break; }
        }
    }
}