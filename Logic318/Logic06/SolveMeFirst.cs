﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class SolveMeFirst
    {
        public SolveMeFirst()
        {
            Console.Write("Masukkan nilai a : ");
            int na = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai b : ");
            int nb = int.Parse(Console.ReadLine());
            Console.WriteLine($"Hasil {na}+{nb}={Additiona(na,nb)}");
        }

        //polymorphis - overloading
        public int Additiona(int value1, int value2) { return value1 + value2; }
        public int Additiona(String value1, int value2) { return value2; }
        public int Additiona(int value1, int value2, int value3) {return value1 + value2+value3;}
    }
}
