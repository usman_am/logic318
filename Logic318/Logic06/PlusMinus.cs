﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class PlusMinus
    {
        public PlusMinus() {
            Console.WriteLine("== Plus Minus ==");
            Console.Write("Number : ");
            String[] number = Console.ReadLine().Split(" ");
            int[] arrayi = Array.ConvertAll<String, int>(number, int.Parse);
            float nminus = 0;
            float nplus = 0;
            float nzero = 0;
            foreach(int i in arrayi)
            {
                if (i < 0) { nminus++; }
                else if(i > 0) { nplus++; }
                else { nzero++; }
            }
            Console.WriteLine($"{nplus / number.Length}");
            Console.WriteLine($"{nminus / number.Length}");
            Console.WriteLine($"{nzero / number.Length}");
        }
    }
}
