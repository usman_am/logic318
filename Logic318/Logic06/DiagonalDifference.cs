﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic06
{
    internal class DiagonalDifference
    {
        public DiagonalDifference() {
            Console.WriteLine("== Diagonal Difference ==");
            Console.Write("Row x Column : ");
            int number = int.Parse(Console.ReadLine());
            String[,] arrayi = new String[number,number];
            for(int i=0; i<arrayi.GetLength(0); i++)
            {
                Console.Write("Number : ");
                String number2 = Console.ReadLine();
                String[] arraytamp = number2.Split(" ");
                for(int j=0; j<arraytamp.Length ; j++)
                {
                    arrayi[i, j] = arraytamp[j];
                }
               
            }
            Printing.Array2Dim(arrayi);
            String diag1 = "";
            String diag2 = "";
            int hasil1 = 0;
            int hasil2 = 0;
            for(int i = 0; i < arrayi.GetLength(0); i++)
            {
                for(int j = 0; j < arrayi.GetLength(1); j++)
                {
                    if (i== j)
                    {
                        diag1 += arrayi[i,j]+"+";
                        hasil1 += int.Parse(arrayi[i, j]);
                    }
                    if(i+j== arrayi.GetLength(1) - 1)
                    {
                        diag2 += arrayi[i,j]+"+";
                        hasil2 += int.Parse(arrayi[i, j]);
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine($"{diag1.Substring(0,diag1.Length-1)}={hasil1}");
            Console.WriteLine($"{diag2.Substring(0, diag2.Length - 1)}={hasil2}");
            Console.WriteLine($"Hasil {hasil1}-{hasil2}={hasil1-hasil2}");
            //int[] splitInt = Array.ConvertAll<String, int>(arrayi, int.Parse);
            //for (in)

            //for(int i = 0; i < arrayi.GetLength(0); i++)
            //{
            //    for(int j = 0; j < arrayi.GetLength(1); j++)
            //    {
            //        if (i== j)
            //        {
            //            arrayi[i,j] = "X";
            //        }
            //        if(i+j== arrayi.GetLength(1) - 1)
            //        {
            //            arrayi[i,j] = "Y";
            //        }
            //    }
            //}
            //Printing.Array2Dim(arrayi);
        }
    }
}
