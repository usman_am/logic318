﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class Staircase
    {
        public Staircase() {
            Console.WriteLine("== Staircase ==");
            Console.Write("N : ");
            int number = int.Parse(Console.ReadLine());
            String[,] arraya = new string[number,number];
            for (int i = 0; i < number; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < number; j++)
                {
                    //if (j < number - 1 - i) { Console.Write(" "); }
                    //else { 
                    //    Console.Write("#"); 
                    //}
                    if (i+j == number - 1 || i==number-1 || j==number-1) { Console.Write("#"); }
                    else{ Console.Write(" "); }
                }
            }
        }
    }
}
