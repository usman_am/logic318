﻿namespace Logic06
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Program Pertemuan 6");
                Console.WriteLine("1.Solve Me First");
                Console.WriteLine("2.Time Conversion");
                Console.WriteLine("3.Simple Array Sum");
                Console.WriteLine("4.Diagonal Defference");
                Console.WriteLine("5.Plus Minus");
                Console.WriteLine("6.Staircase");
                Console.WriteLine("7.Mini-Max Sum");
                Console.WriteLine("8.Birthday Cake Candles");
                Console.WriteLine("9.A Very Big Sum");
                Console.WriteLine("10.Compare The Triplets");
                Console.WriteLine("0.Menu Utama");
                Console.Write("Pilih : ");
                int pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 0:
                        break;
                    case 1:
                       SolveMeFirst hasil01 = new SolveMeFirst();
                        break;
                    case 2:
                        TimeConversion hasil02 = new TimeConversion();
                        break;
                    case 3:
                        SimpleArraySum hasil03 = new SimpleArraySum();
                        break;
                    case 4:
                        DiagonalDifference hasil04 = new DiagonalDifference();
                        break;
                    case 5:
                        PlusMinus hasil05 = new PlusMinus();
                        break;
                    case 6:
                        Staircase hasil06 = new Staircase();
                        break;
                    case 7:
                        MinMaxSum hasil07 = new MinMaxSum();
                        break;
                    case 8:
                        BirthdayCakeCandles hasil08 = new BirthdayCakeCandles();
                        break;
                    case 9:
                        AVeryBigSum hasil09 = new AVeryBigSum();
                        break;
                    case 10:
                        CompreTheTriplets hasil10 = new CompreTheTriplets();
                        break;
                    default: Console.WriteLine("Pilih 1-10"); break;
                }

                if (pilih == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin megulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
        }
    }
}