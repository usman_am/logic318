﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic06
{
    internal class Diagonal
    {
        
        public Diagonal() {
            Console.WriteLine("== Diagonal ==");
            Console.Write("Row x Column : ");
            int number = int.Parse(Console.ReadLine());
            String[,] arrayi = new String[number, number];

            for (int i = 0; i < arrayi.GetLength(0); i++)
            {
                for (int j = 0; j < arrayi.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        arrayi[i, j] = "X";
                    }
                    if (i + j == arrayi.GetLength(1) - 1)
                    {
                        arrayi[i, j] = "Y";
                    }
                }
            }
            Printing.Array2Dim(arrayi);
        }
    }
}
