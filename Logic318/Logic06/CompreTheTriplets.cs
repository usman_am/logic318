﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class CompreTheTriplets
    {
        public CompreTheTriplets() {
            //Sample Input 0
            //5 6 7
            //3 6 10
            //Sample Output 0
            //1,1
            // compare
            Console.WriteLine("== Compare The Triplets ==");
            Console.Write("First Number : ");
            int[] number0 = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Console.Write("Second Number : ");
            int[] number1 = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            int n1 = 0; int n2 = 0;
            for(int i = 0; i < number0.Length; i++)
            {
                if (number0[i] > number1[i]) { n1++; }
                else if (number0[i] < number1[i]) { n2++; }
            }
            Console.WriteLine($"{n1} : {n2}");
        }
    }
}
