﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class BirthdayCakeCandles
    {
        public BirthdayCakeCandles() {
            //Sample Input 0
            //4
            //3 2 1 3
            //Sample Output 0
            //2
            // highest number is 3 and there is 2 of it
            Console.WriteLine("== Birthday Cake Candles ==");
            Console.Write("Number : ");
            String[] number = Console.ReadLine().Split(" ");
            int[] arrayi = Array.ConvertAll<String, int>(number, int.Parse);
            int hn = 0;
            int hnm = 0;
            for(int i = 0; i < arrayi.Length; i++)
            {
                if (arrayi[i] > hn)
                {
                    hn = arrayi[i];
                    hnm = 1;
                }else if (arrayi[i] == hn) { hnm++; }
            }
            Console.WriteLine(hnm);
        }
    }
}
