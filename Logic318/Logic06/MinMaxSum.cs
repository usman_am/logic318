﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class MinMaxSum
    {
        public MinMaxSum() {
            //input
            //1 2 3 4 5
            //output
            //10 14
            Console.WriteLine("== Mini-Max Sum ==");
            Console.Write("Number : ");
            String[] number = Console.ReadLine().Split(" ");
            int[] arrayi = Array.ConvertAll<String, int>(number, int.Parse);
            int nawal = 0;
            int nakhir = 0;

            Array.Sort(arrayi);
            for (int i = 0; i < arrayi.Length; i++)
            {
                for(int j = 0; j<arrayi.Length; j++)
                {
                    if (i == 0 && j!=i) { nakhir += arrayi[j]; }
                    if (i == arrayi.Length-1 && j!=i) { nawal += arrayi[j]; }
                }
            }
            Console.WriteLine(nawal);
            Console.WriteLine(nakhir);
        }
    }
}
