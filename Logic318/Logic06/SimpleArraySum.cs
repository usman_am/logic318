﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class SimpleArraySum
    {
        public SimpleArraySum() {
            Console.WriteLine("== Simple Array Sum ==");
            Console.Write("Number : ");
            String number = Console.ReadLine();
            String[] arrayi = number.Split(" ");
            int[] splitInt = Array.ConvertAll<String, int>(arrayi, int.Parse);
            int tamp = 0;
            for(int i = 0; i < arrayi.Length; i++)
            {
                Console.Write(arrayi[i] );
                tamp += int.Parse(arrayi[i]);
                if (i != arrayi.Length - 1)
                {
                    Console.Write(" + ");
                }
            }
            Console.Write($" = {tamp}");
        }
    }
}
