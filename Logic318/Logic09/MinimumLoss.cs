﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class MinimumLoss
    {
        public MinimumLoss()
        {
            Console.WriteLine("== Minimum Loss ==");
            Console.Write("Range price on year : ");
            String[] number1 = Console.ReadLine().Split(" ");
            int[] array1 = Array.ConvertAll<String, int>(number1, int.Parse);
            int nTamp = 100;
            for(int i = 0; i < array1.Length; i++)
            {
                for(int j = i+1; j < array1.Length; j++)
                {
                    if (array1[i] - array1[j] >= 0 && array1[i] - array1[j] < nTamp) { nTamp = array1[i] - array1[j]; }
                }
            }
            Console.WriteLine(nTamp);
        }
    }
}
