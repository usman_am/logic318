﻿namespace Logic09
{
    internal class Pairs
    {
        public Pairs()
        {
            Console.WriteLine("== Pairs ==");
            Console.Write("Target : ");
            int target = int.Parse(Console.ReadLine());
            Console.Write("Range number : ");
            String[] number1 = Console.ReadLine().Split(" ");
            int[] array1 = Array.ConvertAll<String, int>(number1, int.Parse);
            int nTamp = 0;
            for(int i=0; i<array1.Length; i++)
            {
                for(int j=0; j<array1.Length; j++)
                {
                    if (array1[i] - array1[j]==target && i!=j) { nTamp++; }
                }
            }
            Console.WriteLine(nTamp);
        }
    }
}
