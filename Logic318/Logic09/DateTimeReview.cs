﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class DateTimeReview
    {
        public DateTimeReview()
        {
            // format datetime yyyy-MM-dd
            Console.WriteLine("== Date Time Review ==");
            Console.Write("Tanggal awal  : ");
            string[] d1 = Console.ReadLine().Split(' ');
            string[] dtawal = d1[0].Split("/");
            string[] timeawal = d1[1].Split(":");
            DateTime awal = new DateTime(int.Parse(dtawal[0]), int.Parse(dtawal[1]), int.Parse(dtawal[2]), 
                int.Parse(timeawal[0]), int.Parse(timeawal[1]), int.Parse(timeawal[2]));

            Console.Write("Tanggal akhir  : ");
            string[] d2 = Console.ReadLine().Split(' ');
            string[] dtakhir = d2[0].Split("/");
            string[] timeakhir = d2[1].Split(":");
            DateTime akhir = new DateTime(int.Parse(dtakhir[0]), int.Parse(dtakhir[1]), int.Parse(dtakhir[2]),
                int.Parse(timeakhir[0]), int.Parse(timeakhir[1]), int.Parse(timeakhir[2]));

            TimeSpan diff = akhir - awal;

            Console.WriteLine($"{awal.ToString("yyyy-MM-dd hh:mm:ss")}-{akhir.ToString("yyyy-MM-dd hh:mm:ss")}");
            Console.WriteLine($"Selisih tahun {diff.Days/360}");
            Console.WriteLine($"Selisih hari {diff.Days}");
            Console.WriteLine($"Selisih jam {diff.Hours}");
            Console.WriteLine($"Selisih menit {diff.Minutes}");
            Console.WriteLine($"Selisih detik {diff.Seconds}");
            Console.WriteLine($"Selisih total jam {diff.TotalHours}");


        }
    }
}
