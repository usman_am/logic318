﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class SherlockandArray
    {
        public SherlockandArray()
        {
            Console.WriteLine("== Sherlock and Array ==");
            Console.Write("number1 : ");
            String[] number1 = Console.ReadLine().Split(" ");
            int[] array1 = Array.ConvertAll<String, int>(number1, int.Parse);
            bool hasil = false;
            for(int i=0; i<array1.Length; i++)
            {
                int nleft = 0;
                int nright = 0;
                for(int j=0; j<i; j++) { nleft += array1[j]; }
                for(int k=array1.Length-1; k>i; k--) { nright += array1[k]; }
                if(nleft == nright) { hasil = true; break; }
            }
            if(hasil==true) { Console.WriteLine("YES"); } else { Console.WriteLine("NO"); }
        }
    }
}
