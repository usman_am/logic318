﻿namespace Logic09
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Program Pertemuan 9");
                Console.WriteLine("1.Missing Number");
                Console.WriteLine("2.Sharelock an Array");
                Console.WriteLine("3.Ice Cream Parlor");
                Console.WriteLine("4.Date Time Review");
                Console.WriteLine("5.Minimum Loss");
                Console.WriteLine("6.Pairs");
                Console.WriteLine("0.Main Menu");
                Console.Write("Pilih : ");
                int pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 0: break;
                    case 1: MissingNumber program01 = new MissingNumber(); break;
                    case 2: SherlockandArray program02 = new SherlockandArray(); break;
                    case 3: IceCreamParlor program03 = new IceCreamParlor(); break;
                    case 4: DateTimeReview program04 = new DateTimeReview(); break;
                    case 5: MinimumLoss program05 = new MinimumLoss(); break;
                    case 6: Pairs program06 = new Pairs(); break;
                    default: Console.WriteLine("Pilih 1-6"); break;
                }

                if (pilih == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin megulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
        }
    }
}