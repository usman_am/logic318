﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class MissingNumber
    {
        public MissingNumber()
        {
            Console.WriteLine("== Missing Number ==");
            Console.Write("number1 : ");
            String[] number1 = Console.ReadLine().Split(" ");
            //String[] number1 = "203 204 205 206 207 208 203 204 205 206".Split(" ");
            int[] array1 = Array.ConvertAll<String, int>(number1, int.Parse);
            List<int> list1 = array1.ToList();
            Console.Write("number2 : ");
            String[] number2 = Console.ReadLine().Split(" ");
            //String[] number2 = "203 204 204 205 206 207 205 208 203 206 205 206 204".Split(" ");
            int[] array2 = Array.ConvertAll<String, int>(number2, int.Parse);
            List<int> list2 = array2.ToList();
            
            String hasil = "";

            /*for(int i=0; i<list1.Count; i++)
            {
                for(int j=0; j < list2.Count;)
                {
                    if (list1[i] == list2[j])
                    {
                        list1.RemoveAt(i);
                        list2.RemoveAt(j);
                        break;
                    }
                }
            }
            foreach (var item in list1)
            {
                Console.Write(item + " ");
            }
            foreach (var item in list2)
            {
                Console.Write(item + " ");
            }*/
            for (int i = 0; i < array1.Length; i++)
            {
                bool nt = false;
                for (int j = 0; j < array2.Length; j++)
                {
                    if (array1[i] == array2[j])
                    {
                        array2 = array2.Where((source, index) => index != j).ToArray();
                        array1 = array1.Where((source, index) => index != i).ToArray();
                        i--;
                        j--;
                        break;
                    }
                }
            }
            //list1.AddRange(list2);

            array1 = array1.Union(array2).ToArray();
            foreach (var item in array1)
            {
                Console.Write(item + " ");
            }
        }
    }
}
