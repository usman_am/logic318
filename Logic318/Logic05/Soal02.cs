﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal02
    {
        public Soal02()
        {

            Console.Write("N = ");
            int n = int.Parse(Console.ReadLine());
            int counter = 0;
            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{i*2} ");
               
            }
        }
    }
}
