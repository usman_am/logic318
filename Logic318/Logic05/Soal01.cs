﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal01
    {
        public Soal01() {

            Console.Write("N = ");
            int n = int.Parse(Console.ReadLine());
            int counter = 1;
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{i * 2 + 1} ");

            }
        }

        
    }
}
