﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal08
    {
        public Soal08() {
            Console.Write("N = ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{Math.Pow(3,i+1)} ");
            }
        }
    }
}
