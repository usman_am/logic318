﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal09
    {
        public Soal09() {
            Console.Write("N = ");
            int n = int.Parse(Console.ReadLine());
            int counter = 4;
            int ct2 = 0;
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{Math.Pow(4, i + 1)} ");
                if (i % 2 == 1 && i != n - 1)
                {
                    Console.Write($"* ");
                    n -= 1;
                }
            }
        }
    }
}
