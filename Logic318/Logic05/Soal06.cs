﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal06
    {
        public Soal06() {
            // 1    5   *   13  17  *   25 
            Console.Write("N = ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                Console.Write(i % 3 != 2 ? $"{ i * 4 + 1 }\t" : "*\t"); //ternary
                //if (i%3 == 2)
                //{
                //    Console.Write($"* ");
                //}
                //else
                //{
                //    Console.Write($"{i * 4 + 1} ");
                //}
            }
        }
    }
}
