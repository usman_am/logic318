﻿namespace Logic05
{
    public class Program
    {
        public Program() { Menu(); }

        public static void Main(String[] args)
        {
            Menu();
            Console.ReadKey();
        }

        private static void Menu()
        {
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Program pertemuan 05 :");
                Console.WriteLine("Pilih soal 1-10");
                Console.WriteLine("0.Menu Utama");
                Console.Write("Pilih : ");
                int n1 = int.Parse(Console.ReadLine());

                switch (n1)
                {
                    case 0:
                        break;
                    case 1:
                        Soal01 hasil1 = new Soal01();
                        break;
                    case 2:
                        Soal02 hasil2 = new Soal02();
                        break;
                    case 3:
                        Soal03 hasil3 = new Soal03();
                        break;
                    case 4:
                        Soal04 hasil4 = new Soal04();
                        break;
                    case 5:
                        Soal05 hasil5 = new Soal05();
                        break;
                    case 6:
                        Soal06 hasil6 = new Soal06();
                        break;
                    case 7:
                        Soal07 hasil7 = new Soal07();
                        break;
                    case 8:
                        Soal08 hasil8 = new Soal08();
                        break;
                    case 9:
                        Soal09 hasil9 = new Soal09();
                        break;
                    case 10:
                        Soal10 hasil10 = new Soal10();
                        break;
                    default:
                        Console.WriteLine("Masukkan angka yang benar");
                        break;
                }
                if (n1 == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin megulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
            
        }


        

    }
}
