﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal07
    {
        public Soal07() {
            Console.Write("N = ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                //Console.Write($"{i * 2 + 2} ");
                Console.Write($"{Math.Pow(2,i+1)} ");
            }
        }
    }
}
