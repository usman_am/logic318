﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal05
    {
        public Soal05() {
            Console.Write("N = ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{i * 4 + 1} ");
                if (i % 2 == 1 && i!=n-1) {
                    Console.Write($"* ");
                    n -= 1;
                }
            }
        }
    }
}
