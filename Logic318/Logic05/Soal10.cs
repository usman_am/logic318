﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal10
    {
        public Soal10() {
            Console.Write("N = ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                //Console.Write(i % 4 != 3 ? $"{Math.Pow(3, i + 1)}\t" : "XXX\t"); //ternary
                if(i%4==3)
                {
                    Console.Write($"{DigitToString(Math.Pow(3, i + 1))} ");
                }
                else
                {
                    Console.Write($"{Math.Pow(3, i + 1)} ");
                }
            }
        }

        private string DigitToString(double n )
        {
            String result = "";
            for(int i = 0; i < n.ToString().Length;i++)
            {
                result += "X";
            }
            return result;
        }
    }
}
