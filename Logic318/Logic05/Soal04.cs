﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
    internal class Soal04
    {
        public Soal04() {
            Console.Write("N = ");
            int n = int.Parse(Console.ReadLine());
            int counter = 1;
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{i * 4 + 1} ");
            }
        }
    }
}
