﻿using Logic02;
//switchcase();
//whileloop();
//whileloop2();
//dowhile();
//forloop();
//forbersarang();
//foreachloop();
//array2dimensi();
//liststring();
//objeklist();
//listmanipulating();
//padleft();
containts();
Console.ReadKey();

static void containts()
{
    Console.Write("masukkan kalimat : ");
    String kalimat = Console.ReadLine();
    Console.Write("masukkan contain : ");
    String contain = Console.ReadLine();
    if (kalimat.Contains(contain))
    {
        Console.WriteLine($"Kalimat ({kalimat}) ini mengandung ({contain})");
    }
    else
    {
        Console.WriteLine($"Kalimat ({kalimat}) ini tidak mengandung ({contain})");
    }
}

static void padleft()
{
    Console.Write("masukkan input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("masukkan panjang karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("masukkan char : ");
    char chars = char.Parse(Console.ReadLine());

    Console.WriteLine($"Hasil padleft : {input.ToString().PadLeft(panjang, chars)}");
}

static void covertArrayAll()
{
    Console.Write("masukkan deret angka degan tada koma");
    int[] array1 = Array.ConvertAll(Console.ReadLine().Split(","),int.Parse);

Console.WriteLine(string.Join("\t", array1));
}
static void listmanipulating()
{
    List<User> Orang = new List<User>()
    {
    new User() { Name = "Ujang Kurung", Age = 18},
    new User() { Name = "Aies Bahari", Age=21},
    new User() { Name = "Ruby yanto", Age = 10},
    new User() { Name = "Sawal manwa", Age=24}
    };

//add list
Orang.Add(new User() { Name = "Rizki Bilar", Age = 80 });
    
    User user = new User();
    user.Name = "marita nia";
    user.Age = 17;
    Orang.Add(user);
    //remove list
    Orang.RemoveAt(0);

    User user2 = Orang.Where(a => a.Name == "Ruby yanto").FirstOrDefault();
    Orang.Remove(user2);

    //insert list
    Orang.Insert(2, new User() { Name="Wahyu yuyu", Age=22});

    for (int i = 0; i < Orang.Count; i++)
    {
        Console.WriteLine(Orang[i].Name+"\t sudah berumur \t" + Orang[i].Age+" tahun");
    }
}

static void objeklist()
{
    List<User> Orang = new List<User>()
    {
          new User() { Name = "Ujang Kurung", Age = 18},
    new User() { Name = "Aies Bahari", Age=21}
    };

    for (int i = 0; i < Orang.Count; i++)
    {
        Console.WriteLine(String.Join(", ", Orang[i].Name, Orang[i].Age));
    }
}

static void liststring()
{
    List<string> list = new List<string>();
    list.Add("a");
    list.Add("b");
    list.Add("c");
    Console.WriteLine(String.Join(", ", list));
    //foreach (var item in list)
    //{
    //    Console.Write(item+" ");
    //}
}

static void array2dimensi()
{
    int[,] ints = new int[,]
    {
        {1,2,3 }, {4,5,6}, {8,9,10}
    };
    
    for(int i = 0; i<ints.GetLength(0); i++)
    {
        for(int j = 0; j < ints.GetLength(1); j++)
        {
            Console.Write(ints[i, j]);
        }
        Console.Write("\n");
    }

    //foreach (var item in ints)
    //{
    //    Console.Write(item);
    //}
}

static void foreachloop()
{
    int[] array1 = { 1, 2, 3 };

    foreach (var item in array1)
    {
        Console.WriteLine($"angka ke {item}");
    }
}

static void forbersarang()
{
    for(int i = 0; i < 3; i++)
    {
        for(int j = 0; j < 3; j++) {
            Console.Write("({0},{1})", i, j);
        }
        Console.Write("\n");
    }
}

static void forloop()
{
    for(int i = 0; i < 10; i++)
    {
        Console.WriteLine(i);
    }
}
static void dowhile()
{
    int a = 0;
    do
    {
        Console.WriteLine($"angka ke {a}");
        a++;
    }while (a < 5);
}

static void whileloop()
{
    int nilai = 1;
    while (nilai < 5)
    {
        Console.WriteLine($"angka {nilai}");
        nilai++;
    }
}

static void whileloop2()
{
    bool status = true;
    while (status) {
            Console.Write("masukkan angka 1 untuk stop : ");
            int input = int.Parse(Console.ReadLine());
            if (input == 1)
            {
                status = false;
            }
    }
}

static void switchcase()
{
    Console.Write("Pilih buah kesukaan anda (apel/jeruk/pisang): ");
    String pilih = Console.ReadLine().ToLower();

    switch(pilih)
    {
        case "apel":
            Console.WriteLine("Ada memilih apel");
            break;
        case "jeruk":
            Console.WriteLine("Ada memilih jeruk");
            break;
        case "pisang":
            Console.WriteLine("Ada memilih pisang");
            break;
        default: Console.WriteLine("anda memilih buah yang lain");
            break;
    }
}