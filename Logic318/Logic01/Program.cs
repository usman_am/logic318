﻿//konversi();
//task1();
//task2();
//ternary();
//manipulasiString();
//stringsplitjoin();
//substring();
Console.ReadKey();

static void substring()
{
    String kata = "bulan ini hujan terus";
    Console.WriteLine(kata.Substring(3, 8));
}

static void stringsplitjoin()
{
    String kata = "kamu membeli telur asin di pasar";
    int[] angka = { 0, 2, 8, 7, 1 };

    String[] array1 = kata.Split(" ");
    String kata2 = String.Join(", ", angka);

    foreach (String s in array1)
    {
        Console.WriteLine(s);
    }
    Console.WriteLine(kata2);
}

static void manipulasiString()
{
    String kata = "jamu tukiem rasanya NIKMAT";
    //pangjang kata
    Console.WriteLine(kata.Length);
    //toUppercase
    Console.WriteLine(kata.ToUpper());
    //to lower
    Console.WriteLine(kata.ToLower());
    //remove
    Console.WriteLine(kata.Remove(10));
    //insert
    Console.WriteLine(kata.Insert(5, "jerman"));
    //replace
    Console.WriteLine(kata.Replace("jamu", "ayam penyet"));
}

static void ternary()
{
    var n1 = 90;
    var n2 = 30;
    var z = (n1 > n2) ? "lebih besar" : "lebih kecil";
    Console.WriteLine(z);
}

static void task2()
{
    Console.Write("Kode Baju = ");
    int kode = int.Parse(Console.ReadLine());
    Console.Write("Kode Ukuran = ");
    String ukuran = Console.ReadLine();

    String merk = "";
    int harga = 0;
    if(kode == 1)
    {
        merk = "IMP";
        if (ukuran == "S")
        {
            harga = 200000;
        }
        else if (ukuran == "M")
        {
            harga = 220000;
        }
        else {
            harga = 250000;
        }
    }else if(kode == 2)
    {
        merk = "Prada";
        if (ukuran == "S")
        {
            harga = 150000;
        }
        else if (ukuran == "M")
        {
            harga = 160000;
        }
        else
        {
            harga = 170000;
        }
    }
    else if (kode == 3)
    {
        merk = "Gucci";
        if (ukuran == "S")
        {
            harga = 200000;
        }
        else if (ukuran == "M")
        {
            harga = 200000;
        }
        else
        {
            harga = 200000;
        }
    }
    Console.WriteLine("Merk Baju = "+merk);
    Console.WriteLine("Harga = "+harga);
}

static void task1()
{
    Console.Write("Mtk = ");
    int mtk = int.Parse(Console.ReadLine());
    Console.Write("Fisika = ");
    int fisika = int.Parse(Console.ReadLine());
    Console.Write("kimia = ");
    int kimia = int.Parse(Console.ReadLine());
    int hasil = (mtk + fisika + kimia) / 3;
    Console.WriteLine("nilai rata-rata = {0}", hasil);
    if(hasil > 50)
    {
        Console.WriteLine("Selamat Kamu Berhasil");
    }
    else
    {
        Console.WriteLine("Maaf kamu gagal");
    }
}

static void operatorlogic()
{
    int umur, password = 0;
    Console.WriteLine("--operator perbandingan--");
    Console.Write("umur : ");
    umur = int.Parse(Console.ReadLine());
    Console.Write("password : ");
    password = int.Parse(Console.ReadLine());

    bool isAdult = umur > 18;
    bool isPasswordValid = password == 123;

    if(isAdult && isPasswordValid)
    {
        Console.WriteLine("anda sudah dewasa dan password valid");
    }else if (isAdult && !isPasswordValid)
    {
        Console.WriteLine("anda sudah dewasa dan password tidak valid");
    }
    else if (!isAdult && isPasswordValid)
    {
        Console.WriteLine("anda belum dewasa dan password valid");
    }

}

static void operatorPerbandingan()
{
    int mangga, apel = 0;
    Console.WriteLine("--operator perbandingan--");
    Console.Write("mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("apel : ");
    apel = int.Parse(Console.ReadLine());

    Console.WriteLine("Hasil perbandingan :");
    Console.WriteLine($"Mangga < apel = {apel < mangga}");
    Console.WriteLine($"Mangga > apel = {apel > mangga}");
    Console.WriteLine($"Mangga = apel = {apel = mangga}");
    Console.WriteLine($"Mangga >= apel = {apel >= mangga}");
}

static void konversi()
{
    String nama = Console.ReadLine();
    Console.WriteLine(nama);
    Console.WriteLine($"Nama = {nama}");
    Console.WriteLine("Nama = {0} {1}", nama, nama);

    int myInt = 10;
    double myDouble = 5.32;
    bool myBool = false;

    int input = int.Parse(Console.ReadLine());
    Console.WriteLine(Convert.ToString(input));
    Console.WriteLine(Convert.ToString(myInt));

}