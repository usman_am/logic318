﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05_part2
{
    public class Soal01
    {
        public Soal01() {
            Console.Write("N : ");
            int n1 = int.Parse(Console.ReadLine());
            Console.Write("N2 : ");
            int n2 = int.Parse(Console.ReadLine());
            String[] arrString = new String[n1];
            for (int i = 0; i < arrString.Length; i++)
            {
                arrString[i] = Math.Pow(n2, i).ToString();
            }
            Printing.Array1Dim(arrString);
        }
    }
}
