﻿using static Logic05_part2.Aarray2D;

namespace Logic05_part2
{
    public class Program
    {
        public Program() { Menu(); }
        public static void Main(String[] args)
        {
            Menu();
            Console.ReadKey();
        }

        private static void Menu()
        {
            while (true)
            {
                Console.WriteLine(""); 
                Console.WriteLine("Program Pertemuan 5 core 2");
                Console.WriteLine("Pilih soal 1-10");
                Console.WriteLine("0.Menu Utama");
                Console.Write("Pilih : ");
                int pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 0:
                        break;
                    case 1:
                        Soal01 hasil01 = new Soal01();
                        break;
                    case 2:
                        Array2DSoal02 hasil02 = new Array2DSoal02();
                        break;
                    case 3:
                        Array2DSoal03 hasil03 = new Array2DSoal03();
                        break;
                    case 4:
                        Array2DSoal04 hasil04 = new Array2DSoal04();
                        break;
                    case 5:
                        Array2DSoal05 hasil05 = new Array2DSoal05();
                        break;
                    case 6:
                        Array2DSoal06 hasil06 = new Array2DSoal06();
                        break;
                    case 7:
                        Array2DSoal07 hasil07 = new Array2DSoal07();
                        break;
                    case 8:
                        Array2DSoal08 hasil08 = new Array2DSoal08();
                        break;
                    case 9:
                        Array2DSoal09 hasil09 = new Array2DSoal09();
                        break;
                    case 10:
                        Array2DSoal10 hasil10 = new Array2DSoal10();
                        break;
                    default:
                        Console.WriteLine("Pilih angka 1-10");
                        break;
                }
                if (pilih == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin mengulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
        }
    }
}