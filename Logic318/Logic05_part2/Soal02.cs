﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05_part2
{
    internal class Soal02
    {
        public Soal02() {
            Console.Write("N : ");
            int n1 = int.Parse(Console.ReadLine());
            Console.Write("N2 : ");
            int n2 = int.Parse(Console.ReadLine());
            String[] arrString = new String[n1];
            for (int i = 0; i < arrString.Length; i++)
            {
                
                if(i % 2 == 3)
                {
                    arrString[i] = "-"+Math.Pow(n2, i).ToString();
                }
                else
                {
                    arrString[i] = Math.Pow(n2, i).ToString();
                }
            }
            Printing.Array1Dim(arrString);
        }
    }
}
