﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05_part2
{
    public class Aarray2D
    {
        public class Array2DSoal01
        {
            public Array2DSoal01() {
                Console.WriteLine("== Soal 01");
                Console.Write("Masukkan nilai n1 : ");
                int n1 = int.Parse(Console.ReadLine());
                Console.WriteLine();
                Console.Write("Masukkan nilai n2 : ");
                int n2 = int.Parse(Console.ReadLine());

                string[,] array = new string[2, n1];

                for(int i=0; i< n1; i++)
                {
                    array[0, i]=i.ToString();
                    array[1, i] = Math.Pow(n2, i).ToString();
                }
                Printing.Array2Dim(array);
            }
        }

        public class Array2DSoal02{
            public Array2DSoal02()
            {
                Console.WriteLine("== Soal 02");
                Console.Write("Masukkan nilai n1 : ");
                int n1 = int.Parse(Console.ReadLine());
                Console.WriteLine();
                Console.Write("Masukkan nilai n2 : ");
                int n2 = int.Parse(Console.ReadLine());

                string[,] array = new string[2, n1];

                for (int i = 0; i < n1; i++)
                {
                    array[0, i] = i.ToString();
                    if (i % 3 == 2)
                    {
                        array[1, i] ="-"+Math.Pow(n2, i).ToString();
                    }
                    else
                    {
                        array[1, i] = Math.Pow(n2, i).ToString();
                    }
                    
                }
                Printing.Array2Dim(array);
            }
        }

        public class Array2DSoal03
        {
            public Array2DSoal03()
            {
                Console.WriteLine("== Soal 03");
                Console.Write("Masukkan nilai n1 : ");
                int n1 = int.Parse(Console.ReadLine());
                Console.WriteLine();
                Console.Write("Masukkan nilai n2 : ");
                int n2 = int.Parse(Console.ReadLine());

                string[,] array = new string[2, n1];
               int m = n2;
                for (int i = 0; i < n1; i++)
                {
                    array[0, i] = i.ToString();
                    
                    if (i <= n1 / 2)
                    {
                        array[1, i] = (m).ToString();
                        array[1, n1-1-i] = (m).ToString();
                        m *= 2;
                    }
                }
                Printing.Array2Dim(array);
            }
        }

        public class Array2DSoal04
        {
            public Array2DSoal04()
            {
                //n=7, n2=5
                // 0    1   2   3   4   5   6
                // 1    5   2   10  3   15  4
                Console.WriteLine("== Soal 04");
                Console.Write("Masukkan nilai n1 : ");
                int n1 = int.Parse(Console.ReadLine());
                Console.WriteLine();
                Console.Write("Masukkan nilai n2 : ");
                int n2 = int.Parse(Console.ReadLine());

                string[,] array = new string[2, n1];
                for (int i = 0; i < n1; i++)
                {
                    array[0, i] = i.ToString();
                    if (i %2 ==0)
                    { //ternary
                        array[1, i] = i == 0 ? 1.ToString():(int.Parse(array[1,i-2])+1).ToString();
                    }
                    else
                    {
                        array[1, i] = i == 1 ? n2.ToString():(int.Parse(array[1, i - 2])+n2).ToString();
                    }
                }
                Printing.Array2Dim(array);
            }
        }

        public class Array2DSoal05
        {
            public Array2DSoal05()
            {
                //n=7
                // 0    1   2   3   4   5   6
                // 7    8   9   10  11  12  13
                // 14   15  16  17  18  19  20
                
                Console.WriteLine("== Soal 05");
                Console.Write("Masukkan nilai N : ");
                int n1 = int.Parse(Console.ReadLine());
                Console.WriteLine();

                string[,] array = new string[3, n1];
                for (int i = 0; i < n1; i++)
                {
                    array[0, i] = i.ToString();
                    array[1, i] = (n1+i).ToString();
                    array[2, i] = (n1+n1+i).ToString();
                }
                Printing.Array2Dim(array);
            }
        }

        public class Array2DSoal06
        {
            public Array2DSoal06()
            {
                //n=7
                // 0    1   2   3    4     5      6
                // 1    7   49  343  2401  16807  117649
                // 1    8   51  346  2405  16812  117655

                //n = 5
                //0   1   2   3     4
                //1   5   25  125   625
                //1   6   27  128   629
                Console.WriteLine("== Soal 06");
                Console.Write("Masukkan nilai N : ");
                int n1 = int.Parse(Console.ReadLine());
                Console.WriteLine();

                string[,] array = new string[3, n1];
                for (int i = 0; i < n1; i++)
                {
                    array[0, i] = i.ToString();
                    array[1, i] = (Math.Pow(n1, i)).ToString();
                    array[2, i] = (Math.Pow(n1, i)+i).ToString();
                }
                Printing.Array2Dim(array);
            }
        }

        public class Array2DSoal07
        {
            public Array2DSoal07()
            {
                //n=7
                // 0    1   2   3   4   5   6
                // 7    8   9   10  11  12  13
                // 14   15  16  17  18  19  20
                Console.WriteLine("== Soal 07");
                Console.Write("Masukkan nilai N : ");
                int n1 = int.Parse(Console.ReadLine());
                Console.WriteLine();

                string[,] array = new string[3, n1];
                for (int i = 0; i < n1; i++)
                {
                    array[0, i] = i.ToString();
                    array[1, i] = (n1 + i).ToString();
                    array[2, i] = (n1 + n1 + i).ToString();
                }
                Printing.Array2Dim(array);
            }
        }

        public class Array2DSoal08
        {
            public Array2DSoal08()
            {
                //n=7
                // 0    1   2   3   4   5   6
                // 0    2   4   6   8   10  12
                // 0    3   6   9  12   15  18  
                Console.WriteLine("== Soal 08");
                Console.Write("Masukkan nilai N : ");
                int n1 = int.Parse(Console.ReadLine());
                Console.WriteLine();

                string[,] array = new string[3, n1];
                for (int i = 0; i < n1; i++)
                {
                    array[0, i] = i.ToString();
                    array[1, i] = (i*2).ToString();
                    array[2, i] = (i*3).ToString();
                }
                Printing.Array2Dim(array);
            }
        }

        public class Array2DSoal09
        {
            public Array2DSoal09()
            {
                //n=7, n2=3
                // 0    1   2   3   4   5   6
                // 0    3   6   9  12   15  18  
                // 18   15  12  9  6    3   0
                Console.WriteLine("== Soal 09");
                Console.Write("Masukkan nilai N : ");
                int n1 = int.Parse(Console.ReadLine());
                Console.Write("Masukkan nilai N2 : ");
                int n2 = int.Parse(Console.ReadLine());
                Console.WriteLine();
                string[,] array = new string[3, n1];
                for (int i = 0; i < n1; i++)
                {
                    array[0, i] = i.ToString();
                    array[1, i] = (i*n2).ToString();
                    array[2, i] = ((n1-i-1) *n2).ToString();
                }
                Printing.Array2Dim(array);
            }
        }

        public class Array2DSoal10
        {
            public Array2DSoal10()
            {
                //n=7, n2=3
                // 0    1   2   3   4   5   6
                // 0    3   6   9   12  15  18  
                // 0    4   8   12  16  20  24
                Console.WriteLine("== Soal 10");
                Console.Write("Masukkan nilai N : ");
                int n1 = int.Parse(Console.ReadLine());
                Console.Write("Masukkan nilai N2 : ");
                int n2 = int.Parse(Console.ReadLine());
                Console.WriteLine();
                string[,] array = new string[3, n1];
                for (int i = 0; i < n1; i++)
                {
                    array[0, i] = i.ToString();
                    array[1, i] = (i * n2).ToString();
                    array[2, i] = (i * n2+i).ToString();
                }
                Printing.Array2Dim(array);
            }
        }
    }
}
