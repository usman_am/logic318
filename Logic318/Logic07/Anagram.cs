﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Logic07
{
    internal class Anagram
    {
        public Anagram()
        {
            //Sample Input
            //cde
            //abc

            //Sample Output
            //4
            Console.WriteLine("== Making Anagrams ==");
            Console.Write("number1 : ");
            string n1 = Console.ReadLine();
            Console.Write("number2 : ");
            string n2 = Console.ReadLine();
            int a = 0;

            /*List<Char> list1 = n1.ToList();
            List<Char> list2 = n2.ToList();

            int sum = list1.Count+list2.Count;
            int tot = 0;
            foreach (char item in list1)
            {
                if (list2.Contains(item))
                {
                    list2.Remove(item);
                    tot++;
                }
            }
            Console.WriteLine(sum-tot*2);*/
            for (int i = 0; i < n1.Length; i++)
            {
                for (int j = 0; j < n2.Length; j++)
                {
                    if (n1[i] == n2[j])
                    {
                        n2 = n2.Remove(j, 1); // n1=n1.Remove(i, 1);
                        Console.WriteLine($"{n1},{n2},{n2.Length}");
                    }
                }
            }
            if (n1 == n2) { Console.WriteLine(0); }
            else
            {
                Console.WriteLine($"{n2.Length+n1.Length}");
            }
        
               
        }
    }

}