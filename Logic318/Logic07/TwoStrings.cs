﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class TwoStrings
    {
        public TwoStrings() {
            Console.WriteLine("== Two Strings ==");
            Console.Write("N : ");
            int n = int.Parse(Console.ReadLine());
            List<bool> list = new List<bool>();
            for(int i = 0; i < n; i++)
            {
                Console.Write("text1 : ");
                String nt1 = Console.ReadLine();
                Console.Write("text2 : ");
                String nt2 = Console.ReadLine();
                list.Add(Cek(nt1, nt2));
            }
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
        }

        public static bool Cek(string a, String b) {

            for(int i=0; i<a.Length; i++)
            {
                for(int j=0; j<b.Length; j++)
                {
                    if (a[i] == b[j])
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
