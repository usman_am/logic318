﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class Pangram
    {
        public Pangram() {
            Console.WriteLine("== Hackerrank in a String ==");
            Console.Write("Message : ");
            String message = Console.ReadLine().ToLower();
            char[] text = "abcdefghijklmnopqrstuvwxyz".ToArray();

            int index = 0;
            for (int i = 0; i < text.Length; i++)
            {
                if (message.Contains(text[i])) { index++; }
                else { continue; }
            }
            
            Console.WriteLine(index == text.Length ? "Pangram" : "No Pangram");

            
        }
    }
}
