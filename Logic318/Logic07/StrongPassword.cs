﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class StrongPassword
    {
        public StrongPassword()
        {
            Console.WriteLine("== Strong Password ==");
            Console.Write("Words : ");
            String word = Console.ReadLine();

            String numbers = "0123456789";
            String lower_case = "abcdefghijklmnopqrstuvwxyz";
            String upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            String special_characters = "!@#$%^&*()-+";

            if (word.IndexOfAny(numbers.ToCharArray()) == -1) { Console.WriteLine("Password has no number"); }
            if (word.IndexOfAny(lower_case.ToCharArray()) == -1) { Console.WriteLine("Password has no lower case"); }
            if (word.IndexOfAny(upper_case.ToCharArray()) == -1) { Console.WriteLine("Password has no lower case"); }
            if (word.IndexOfAny(special_characters.ToCharArray()) == -1) { Console.WriteLine("Password has no symbol"); }
        }
    }
}
