﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class CaesarCipher
    {
        public CaesarCipher()
        {
            Console.WriteLine("== Caesar Cipher ==");
            Console.Write("Words : ");
            String word = Console.ReadLine();
            Console.Write("N : ");
            int n = int.Parse(Console.ReadLine());

            char[] arrayChar = word.ToCharArray();
            string result = "";
            for (int i = 0; i < word.Length; i++)
            {
                char tamp = arrayChar[i];
                if (tamp >= 97 && tamp <= 122)
                {
                    tamp = (char)(tamp + n);
                    if (tamp > 'z') // - 26 if past z.
                    {
                        tamp = (char)(tamp - 26);
                    }
                    else if (tamp < 'a') // + 26 if below a.
                    {
                        tamp = (char)(tamp + 26);
                    }
                }else
                if (tamp >= 65 && tamp <= 90)
                {
                    tamp = (char)(tamp + n);
                    if (tamp > 'Z') 
                    {
                        tamp = (char)(tamp - 26);
                    }
                    else if (tamp < 'A') 
                    {
                        tamp = (char)(tamp + 26);
                    }
                }
                result += tamp.ToString();
            }
            Console.WriteLine(result);
        }
    }
}
