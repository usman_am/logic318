﻿namespace Logic07
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Program Pertemuan 7");
                Console.WriteLine("1.CamelCase");
                Console.WriteLine("2.Strong Password");
                Console.WriteLine("3.Caesar Cipher");
                Console.WriteLine("4.Mars Exploration");
                Console.WriteLine("5.Hackerrank In a String");
                Console.WriteLine("6.Pangram");
                Console.WriteLine("7.Separate The Numbers");
                Console.WriteLine("8.Genstone");
                Console.WriteLine("9.Anagram");
                Console.WriteLine("10.Two Strings");
                Console.WriteLine("0.Menu Utama");
                Console.Write("Pilih : ");
                int pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 0: break;
                    case 1:
                        CamelCase hasil01 = new CamelCase();
                        break;
                    case 2:
                        StrongPassword hasil02 = new StrongPassword();
                        break;
                    case 3:
                        CaesarCipher hasil03 = new CaesarCipher();
                        break;
                    case 4:
                        MarsExploration hasil04 = new MarsExploration();
                        break;
                    case 5:
                        HackerrankInAString hasil05 = new HackerrankInAString();
                        break;
                    case 6:
                        Pangram hasil06 = new Pangram();
                        break;
                    case 7:
                        SeparateTheNumbers hasil07 = new SeparateTheNumbers();
                        break;
                    case 8:
                        Gamstone hasil08 = new Gamstone();
                        break;
                    case 9:
                        Anagram hasil09= new Anagram();
                        break;
                    case 10:
                        TwoStrings hasil10 = new TwoStrings();
                        break;
                    default: Console.WriteLine("Pilih 1-10"); break;
                }

                if (pilih == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin megulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
        }
    }
}