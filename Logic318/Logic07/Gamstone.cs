﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class Gamstone
    {
        public Gamstone() {
            ///abcdde
            //baccd
            //eeabg
            Console.WriteLine("== Gemstone ==");
            Console.Write("Jumlah Batu : ");
            int n = int.Parse(Console.ReadLine());
            String[] arraygem = new string[n];
            for(int i=0; i<n; i++)
            {
                Console.Write($"text {i+1} : ");
                arraygem[i] = Console.ReadLine();
            }
            int count, CountGems = 0;
            for(char i = 'a';  i <= 'z'; i++) {
                count = 0;
                for(int j = 0; j < arraygem.Length; j++)
                {
                    if (arraygem[j].Contains(i))
                        count++;
                }
                if(count == arraygem.Length)
                    CountGems++;
            }
            Console.WriteLine(CountGems);
        }
    }
}
