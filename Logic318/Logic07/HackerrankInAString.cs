﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class HackerrankInAString
    {
        public HackerrankInAString()
        {
            Console.WriteLine("== Hackerrank in a String ==");
            Console.Write("Message1 : ");
            String kalimat = Console.ReadLine().ToLower();
            String alfabet = "hackerrank";
            int index = 0;
            
           
            for(int i=0; i<kalimat.Length; i++)
            {
                if (index<alfabet.Length && kalimat[i]== alfabet[index])
                {
                    index++;
                }
            }
            Console.WriteLine(index == alfabet.Length ? "YES" : "NO"); 
           
        }
    }
}
