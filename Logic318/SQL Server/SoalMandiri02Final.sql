create database klinikMega
use klinikMega
--drop database klinikMega

--drop table Pasien
create table Pasien(ID_Pasien varchar(5) primary key,
NIK varchar(25) not null, Nama_Depan varchar(15) not null,
Nama_Belakang varchar(15) null, TTL varchar(200) not null, 
Jenis_kelamin varchar(1) not null);

create table Dokter(ID_Dokter int identity(1,1),
[STR] varchar(10) primary key,
Nama_Depan varchar(15) not null, Nama_Belakang varchar(25) null,
Spesialis varchar(15) not null);

create table Rekam_Medis(ID_Rekam_Medis varchar(5) primary key, 
ID_Pasien varchar(5) foreign key references Pasien(ID_Pasien), 
[STR] varchar(10) foreign key references Dokter([STR]), 
Tanggal datetime not null, 
Diagnosa varchar(500) not null, Status bit not null);

create table Obat(ID_Obat varchar(5) primary Key, 
Nama_Obat varchar(35) not null,Stok int not null default(0), 
Harga money not null, Tanggal_Kadaluarsa date,
Tipe_Obat varchar(10) default 'Sirup');

create table Resep(ID_Resep varchar(5) primary key,
Tanggal_Resep datetime not null, 
ID_Pasien varchar(5) foreign key references Pasien(ID_Pasien),
[STR] varchar(10) foreign key references Dokter([STR]));

create table Transaksi_Obat(ID_Transaksi_Obat varchar(5) primary key,
ID_Obat varchar(5) foreign key references Obat(ID_Obat), 
ID_Resep varchar(5) foreign key references Resep(ID_Resep), 
Quantity int not null, Harga money not null);

create table Perawatan(ID_Perawatan varchar(5) primary key,
ID_Pasien varchar(5) foreign key references Pasien(ID_Pasien),
[STR] varchar(10) foreign key references Dokter([STR]),
Biaya_Perawatan money not null);

--drop table Invoice
create table Invoice(ID int identity(1,1), ID_Invoice varchar(5) primary key, 
Tanggal_Invoice datetime not null,
ID_Resep varchar(5) foreign key references Resep(ID_Resep), 
ID_Perawatan varchar(5) foreign key references Perawatan(ID_Perawatan));


insert into Pasien(ID_Pasien,NIK,Nama_Depan,Nama_Belakang,TTL,Jenis_Kelamin)
values('P0001','16900511970001','Irfan','Hakim','Bekasi, 15-10-2000','L'), 
('P0002','16901109100002','Sumiati',null,'Bekasi, 11-09-2010','P'),
('P0003','16111508010001','Imam','Syafii','Papua, 15-08-2001','L'), 
('P0004','16900511970001','Irfan','Hakim','Bekasi, 15-10-2000','L'), 
('P0005','16900910990003','Joko','Ngawi Ismail','Jakarta Selatan, 09-10-1999','L'),
('P0006','17211510200001','Istiani',null,'Medan, 15-10-2000','P'),
('P0007','16900511970001','Irfan','Hakim','Bekasi, 15-10-2000','L'),
('P0008','16901109100002','Sumiati',null,'Bekasi, 11-09-2010','P'),
('P0009','16900803120002','Rika','Damayanti','Ulak Rengas, 08-03-2012','P'),
('P0010','16111508010001','Imam','Syafii','Papua, 15-08-2001','L');


UPDATE Pasien SET Nama_Belakang = 'Bachdim' WHERE ID_Pasien= 'P0004'
UPDATE Pasien SET Nama_Depan = 'Lukman' WHERE ID_Pasien= 'P0007'
UPDATE Pasien SET Nama_Depan = 'Astuti' WHERE ID_Pasien= 'P0008'
UPDATE Pasien SET Nama_Depan = 'M.' WHERE ID_Pasien= 'P0010'

insert into Dokter([STR],Nama_Depan,Nama_Belakang,Spesialis)
values('D0001','Dr. Rizal','Saputra S.Gz.','Gizi'),
('D0002','Dr. Irfan','Hakim','Umum'),
('D0003','Dr. Sofiya',null,'Umum'),
('D0004','Dr. Kajal','Kuranova S.Keb.','Kandungan'),
('D0005','Dr. Syifa','Almaruddin Sp.KK.','Kulit'),
('D0006','Dr. Hajri','Yansah S.Kg.','Gigi');

insert into Rekam_Medis(ID_Rekam_Medis,ID_Pasien,[STR],Diagnosa,[Status],Tanggal)
values('RM001','P0001','D0005','Pemakaian obat jerawat',0,'20230519'),
('RM002','P0002','D0004','Periksa Kandungan',0,'20230519'),
('RM003','P0003','D0006','Periksa Gigi Berlubang',1,'20230520'),
('RM004','P0004','D0005','Lanjut Pemakaian obat jerawat',0,'20230525'),
('RM005','P0005','D0003','Deman berdarah',1,'20230521'),
('RM006','P0006','D0003','Insomnia',1,'20230524'),
('RM007','P0007','D0005','Keluhan Jerawat Selesai',1,'20230602'),
('RM008','P0008','D0004','Periksa Kandungan',1,'20240101'),
('RM009','P0009','D0006','Pemasangan Kawat Gigi',1,'20240104'),
('RM010','P0010','D0003','Periksa Kesehatan',1,'20240104');

insert into Obat(ID_Obat,Nama_Obat,Stok,Harga,Tanggal_Kadaluarsa)
values('MC001','Obat Jerawat',120,15000,'20251110'),
('MC002','Obat Flu',100,5000,'20221010'),
('MC003','Obat Demam Berdarah',50,150000,'20201110'),
('MC004','Obat Demam',10,25000,'20230909'),
('MC005','Obat Kurang Tidur',40,10000,'20251225');

insert into Resep(ID_Resep,Tanggal_Resep,ID_Pasien,[STR])
values('RS001','20230519','P0001','D0005'),
('RS002','20230521','P0005','D0003'),
('RS003','20230525','P0004','D0005'),
('RS004','20230524','P0006','D0003'),
('RS005','20230104','P0010','D0003');

insert into Transaksi_Obat(ID_Transaksi_Obat,ID_Obat,ID_Resep,Quantity,Harga)
values
('TR001','MC004','RS001',2,15000),
('TR002','MC001','RS002',4,5000),
('TR003','MC003','RS003',3,150000),
('TR004','MC005','RS004',25,25000),
('TR005','MC001','RS005',8,25000),
('TR006','MC004','RS005',5,10000);

insert into Perawatan(ID_Perawatan, ID_Pasien, [STR], Biaya_Perawatan)
values('RM001','P0001','D0005',50000),
('RM002','P0002','D0004',150000),
('RM003','P0003','D0006',50000),
('RM004','P0001','D0005',50000),
('RM005','P0005','D0003',50000),
('RM006','P0006','D0003',20000),
('RM007','P0007','D0005',50000),
('RM008','P0008','D0004',150000),
('RM009','P0009','D0006',250000),
('RM010','P0010','D0003',20000);

insert into Invoice(ID_Invoice,ID_Resep,ID_Perawatan, Tanggal_Invoice)
values ('IV001','RS001','RM001','20230519'),('IV002',null,'RM002','20230519'),
('IV003',null,'RM003','20230520'),('IV004','RS003','RM004','20230525'),
('IV005','RS002','RM005','20230521'), ('IV006','RS004','RM006','20230524'),
('IV007',null,'RM007','20230602'),('IV008',null,'RM008','20240101'),
('IV009',null,'RM008','20240104'),('IV010','RS005','RM010','20240104');

select *
from Pasien p
left join Resep r on r.ID_Pasien=p.ID_Pasien
left join Transaksi_Obat tr on tr.ID_Resep =r.ID_Resep 
left join Obat o on o.ID_Obat=tr.ID_Obat


--DDL Perbaiki Masalah Dibawah Ini
--1. Tambahkan Kolom Jenis Kelamin dengan Tipe data Char(1) pada Tabel Pasien, dan pastikan inputan hanya menerima P atau L 
ALTER TABLE Pasien
ADD Jenis_Kelamin Char(1) not null check(Jenis_Kelamin = 'L' or Jenis_Kelamin ='P')
--2. Ubahlah Kolom Diagnosis menjadi Varchar(255) dan tambahkan kolom status dengan input '1' untuk sembuh dan '0' untuk masa perawatan
ALTER TABLE Rekam_Medis
ADD [Status] bit

ALTER TABLE Rekam_Medis
ALTER COLUMN Diagnosis Varchar(255)
--3. Tambahkan Kolom tipeObat pada table obat dengan tipe data varchar(30) dengan default 'Sirup'
ALTER TABLE Obat
ADD Tipe_Obat Varchat(30) DEFAULT 'Sirup'
--4. Tambahkan Kolom Merk pada table obat dengan tipe data Varchar(50).
ALTER TABLE Obat
ADD Merk Varchar(50) not null
--5. Ubah tipe data TglKadaluarsa menjadi Datetime
ALTER TABLE Obat
ALTER COLUMN TglKadaluarsa Datetime

--DML
--1. Tampilkan Semua Nama Lengkap Pasien yang pernah berobat di klinik
SELECT CONCAT(Nama_Depan,' ', Nama_Belakang) NamaLengkap, 
TTL, DATEDIFF(YEAR, CAST(RIGHT(TTL, 4) AS date), getdate()) Umur
FROM Pasien 

--2. Tamppilkan nama lengkap dokter beserta str nya dan yang termasuk dokter umum dan belum menangani pasien
SELECT CONCAT(dr.Nama_Depan,' ',dr.Nama_Belakang) NamaLengkap,
dr.STR
FROM Dokter dr
LEFT JOIN Rekam_Medis rm ON dr.STR = rm.STR
WHERE dr.Spesialis = 'Umum' AND rm.ID_Rekam_Medis IS NULL

--3. Tampilkan obat yang paling banyak dibeli berserta harganya
SELECT ob.Nama_Obat, ob.Harga, SUM(rp.Quantity) Terjual
FROM Obat ob
JOIN Resep rp ON ob.ID_Obat = rp.ID_Obat 
GROUP BY ob.Nama_Obat, ob.Harga
HAVING SUM(rp.Quantity) = (SELECT MAX(jb.Jumlahterjual)FROM (SELECT * FROM JumlahObatTerjual) jb)

CREATE VIEW JumlahObatTerjual
AS
SELECT ob.ID_Obat IdObat, SUM(rp.Quantity) JumlahTerjual
FROM Obat ob
JOIN Resep rp ON ob.ID_Obat = rp.ID_Obat
GROUP BY ob.ID_Obat

--4. Tampilkan medical record pasien yang ditangani oleh dokter Sofiya
SELECT rm.ID_Rekam_Medis, rm.ID_Pasien, CONCAT(ps.Nama_Depan, ps.Nama_Belakang) NamaPasien,
rm.Diagnosa, CONCAT(dr.Nama_Depan, ' ', dr.Nama_Belakang) Dokter
FROM Rekam_Medis rm
JOIN Dokter dr ON rm.STR = dr.STR
JOIN Pasien ps ON rm.ID_Pasien = ps.ID_Pasien
WHERE dr.STR = 'D0003'

--5. Tampilkan obat yang penjualannya diatas rata-rata 
SELECT ob.Nama_Obat, SUM(ob.Harga*rp.Quantity) JumlahPenjualan
FROM Obat ob
JOIN Resep rp ON ob.ID_Obat = rp.ID_Obat
GROUP BY ob.ID_Obat, ob.Nama_Obat
HAVING SUM(ob.Harga*rp.Quantity) > (SELECT AVG(jb.jpb) FROM 
(SELECT SUM(ob.Harga*rp.Quantity) jpb, ob.ID_Obat 
FROM Obat ob JOIN Resep rp ON ob.ID_Obat = rp.ID_Obat
GROUP BY ob.ID_Obat)jb)

--6. Tampilkan nama obat beserta stocknya dan tanggal expnya yang exp nya lebih dari 1 tahun
SELECT nama_obat, tanggal_kadaluarsa
FROM Obat
WHERE DATEDIFF(YEAR, GETDATE(),Tanggal_Kadaluarsa) > 1


--7. Tampilkan jumlah penjualan obat tiap bulan(nama bulan) per periode tahun
SELECT ob.Nama_Obat, SUM(ob.Harga*rp.Quantity) JumlahPenjualan, 
DATENAME(mm,rp.Tanggal_Resep) Bulan, Year(rp.Tanggal_Resep) Periode
FROM Obat ob
JOIN Resep rp ON ob.ID_Obat = rp.ID_Obat 
GROUP BY ob.ID_Obat, ob.Nama_Obat, MONTH(rp.Tanggal_Resep), DATENAME(mm,rp.Tanggal_Resep), Year(rp.Tanggal_Resep)
ORDER BY Periode, MONTH(rp.Tanggal_Resep)

--8. Tampilkan jumlah pemasukkan klinik pada bulan may di tahun 2023 dan cek apakah diatas penjualan 
--   rata-rata perbulan pada tahun itu
SELECT SUM(Biaya_Perawatan+Total_Pembayaran) JumlahPemasukan,
CASE
	WHEN SUM(Biaya_Perawatan+Total_Pembayaran) < (SELECT * FROM RataRataPendapatanPerbulan) THEN 'YES'
	ELSE 'NO'
END AS Cek
FROM Invoice
WHERE MONTH(Tanggal_Invoice) = 5 AND YEAR(Tanggal_Invoice) = 2023

CREATE VIEW RataRataPendapatanPerbulan
AS
(SELECT AVG(jml.jmlPmbyrn)RataRataPendapatan FROM (SELECT SUM(Biaya_Perawatan+Total_Pembayaran) jmlPmbyrn
FROM Invoice
GROUP BY MONTH(Tanggal_Invoice))jml)

--9. Tampilkan total transaksi per pasien per tahun
SELECT ps.ID_Pasien, CONCAT(ps.Nama_Depan, ' ', ps.Nama_Belakang) NamaLengkap, SUM(inv.Biaya_Perawatan+inv.Total_Pembayaran) TotalTransaksi,
YEAR(inv.Tanggal_Invoice) Tahun
FROM Invoice inv
JOIN Pasien ps ON inv.ID_Pasien = ps.ID_Pasien
GROUP BY ps.ID_Pasien, ps.Nama_Depan, ps.Nama_Belakang, YEAR(inv.Tanggal_Invoice)

--10. Tampilkan Jumlah Pemasukkan tertinggi per periode Tahun
SELECT sub.TotalTransaksi, sub.Bulan, sub.Tahun FROM
(SELECT  SUM(inv.Biaya_Perawatan+inv.Total_Pembayaran) TotalTransaksi,
MONTH(inv.Tanggal_Invoice) Bulan, YEAR(inv.Tanggal_Invoice) Tahun
FROM Invoice inv
JOIN Pasien ps ON inv.ID_Pasien = ps.ID_Pasien
GROUP BY MONTH(inv.Tanggal_Invoice), YEAR(inv.Tanggal_Invoice)) sub
JOIN (SELECT MaxTransaksiPasien FROM MaxTotal) maxTrs  ON sub.TotalTransaksi= maxTrs. MaxTransaksiPasien
JOIN (SELECT Tahun FROM MaxTotal) maxThn  ON sub.Tahun= maxThn.Tahun


ALTER VIEW MaxTotal
AS
SELECT MAX(Inv.TotalTransaksi) MaxTransaksiPasien, Inv.Tahun
FROM
(SELECT MONTH(inv.Tanggal_Invoice) Bulan,YEAR(inv.Tanggal_Invoice) Tahun, SUM(inv.Biaya_Perawatan+inv.Total_Pembayaran) TotalTransaksi
FROM Invoice inv
JOIN Pasien ps ON inv.ID_Pasien = ps.ID_Pasien
GROUP BY  MONTH(inv.Tanggal_Invoice), YEAR(inv.Tanggal_Invoice)) Inv
GROUP BY Inv.Tahun