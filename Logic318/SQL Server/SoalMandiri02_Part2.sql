create database klinikMega
use klinikMega
drop database klinikMega

drop table Pasien
create table Pasien(ID_Pasien varchar(5) primary key,
NIK varchar(25) not null, Nama_Depan varchar(15) not null,
Nama_Belakang varchar(15) null, TTL varchar(200))

create table Dokter(ID_Dokter int identity(1,1),
[STR] varchar(10) primary key,
Nama_Depan varchar(15) not null, Nama_Belakang varchar(25) null,
Spesialis varchar(15) not null);


create table Rekam_Medis(ID_Rekam_Medis varchar(5) primary key, 
ID_Pasien varchar(5) foreign key references Pasien(ID_Pasien), 
[STR] varchar(10) foreign key references Dokter([STR]), 
Diagnosa varchar(500) not null);

create table Obat(ID_Obat varchar(5) primary Key, 
Nama_Obat varchar(35) not null,Stok int not null default(0) check(Stok > 0), 
Harga money not null, Tanggal_Kadaluarsa date);

create table Resep(ID_Resep varchar(5) primary key,
Tanggal_Resep datetime not null, 
ID_Pasien varchar(5) foreign key references Pasien(ID_Pasien),
ID_Obat varchar(5) foreign key references Obat(ID_Obat), 
Quantity int not null);

drop table Invoice
create table Invoice(ID int identity(1,1), ID_Invoice varchar(5) primary key, 
Tanggal_Invoice datetime not null,
ID_Pasien varchar(5) foreign key references Pasien(ID_Pasien), 
Biaya_Perawatan money not null, 
Total_Pembayaran money not null);

insert into Pasien(ID_Pasien,NIK,Nama_Depan,Nama_Belakang,TTL)
values('P0001','16900511970001','Irfan','Hakim','Bekasi, 15-10-2000'), 
('P0002','16901109100002','Sumiati',null,'Bekasi, 11-09-2010'),
('P0003','16111508010001','Imam','Syafii','Papua, 15-08-2001'), 
('P0004','16900511970001','Irfan','Hakim','Bekasi, 15-10-2000'), 
('P0005','16900910990003','Joko','Ngawi Ismail','Jakarta Selatan, 09-10-1999'),
('P0006','17211510200001','Istiani',null,'Medan, 15-10-2000'),
('P0007','16900511970001','Irfan','Hakim','Bekasi, 15-10-2000'),
('P0008','16901109100002','Sumiati',null,'Bekasi, 11-09-2010'),
('P0009','16900803120002','Rika','Damayanti','Ulak Rengas, 08-03-2012'),
('P0010','16111508010001','Imam','Syafii','Papua, 15-08-2001');

insert into Dokter([STR],Nama_Depan,Nama_Belakang,Spesialis)
values('D0001','Dr. Rizal','Saputra S.Gz.','Gizi'),
('D0002','Dr. Irfan','Hakim','Umum'),
('D0003','Dr. Sofiya',null,'Umum'),
('D0004','Dr. Kajal','Kuranova S.Keb.','Kandungan'),
('D0005','Dr. Syifa','Almaruddin Sp.KK.','Kulit'),
('D0006','Dr. Hajri','Yansah S.Kg.','Gigi');

insert into Rekam_Medis(ID_Rekam_Medis,ID_Pasien,[STR],Diagnosa)
values('RM001','P0001','D0005','Pemakaian obat jerawat'),
('RM002','P0002','D0004','Periksa Kandungan'),
('RM003','P0003','D0006','Periksa Gigi Berlubang'),
('RM004','P0001','D0005','Lanjut Pemakaian obat jerawat'),
('RM005','P0005','D0003','Deman berdarah'),
('RM006','P0006','D0003','Insomnia'),
('RM007','P0007','D0005','Keluhan Jerawat Selesai'),
('RM008','P0008','D0004','Periksa Kandungan'),
('RM009','P0009','D0006','Pemasangan Kawat Gigi'),
('RM010','P0010','D0003','Periksa Kesehatan');

insert into Obat(ID_Obat,Nama_Obat,Stok,Harga,Tanggal_Kadaluarsa)
values('MC001','Obat Jerawat',120,15000,'20251110'),
('MC002','Obat Flu',100,5000,'20221010'),
('MC003','Obat Demam Berdarah',50,150000,'20201110'),
('MC004','Obat Demam',10,25000,'20230909'),
('MC005','Obat Kurang Tidur',40,10000,'20251225');

insert into Resep(ID_Resep,Tanggal_Resep,ID_Pasien,ID_Obat,Quantity)
values('RS001','20230519','P0001','MC001',6),
('RS002','20230521','P0005','MC003',2),
('RS003','20230525','P0004','MC001',6),
('RS004','20230524','P0006','MC005',20),
('RS005','20230104','P0010','MC004',20),
('RS006','20230104','P0010','MC002',5);

insert into Invoice(ID_Invoice, Tanggal_Invoice,ID_Pasien,Biaya_Perawatan,Total_Pembayaran)
values ('IV001','20230519','P0001',50000,50000)
update Invoice set Total_Pembayaran = Total_Pembayaran+o.Harga*r.Quantity
from Pasien p
join Resep r on r.ID_Pasien=p.ID_Pasien
join Obat o on o.ID_Obat=r.ID_Obat
where p.ID_Pasien='P0001'

insert into Invoice(ID_Invoice, Tanggal_Invoice,ID_Pasien,Biaya_Perawatan,Total_Pembayaran)
values ('IV002','20230519','P0002',150000,150000)
update Invoice set Total_Pembayaran = Total_Pembayaran+o.Harga*r.Quantity
from Pasien p
join Resep r on r.ID_Pasien=p.ID_Pasien
join Obat o on o.ID_Obat=r.ID_Obat
where p.ID_Pasien='P0002'

insert into Invoice(ID_Invoice, Tanggal_Invoice,ID_Pasien,Biaya_Perawatan,Total_Pembayaran)
values ('IV003','20230520','P0003',50000,50000)
update Invoice set Total_Pembayaran = Total_Pembayaran+o.Harga*r.Quantity
from Pasien p
join Resep r on r.ID_Pasien=p.ID_Pasien
join Obat o on o.ID_Obat=r.ID_Obat
where p.ID_Pasien='P0003'

insert into Invoice(ID_Invoice, Tanggal_Invoice,ID_Pasien,Biaya_Perawatan,Total_Pembayaran)
values ('IV004','20230525','P0004',50000,50000)
update Invoice set Total_Pembayaran = Total_Pembayaran+o.Harga*r.Quantity
from Pasien p
join Resep r on r.ID_Pasien=p.ID_Pasien
join Obat o on o.ID_Obat=r.ID_Obat
where p.ID_Pasien='P0004'

insert into Invoice(ID_Invoice, Tanggal_Invoice,ID_Pasien,Biaya_Perawatan,Total_Pembayaran)
values ('IV005','20230521','P0005',50000,50000)
update Invoice set Total_Pembayaran = Total_Pembayaran+o.Harga*r.Quantity
from Pasien p
join Resep r on r.ID_Pasien=p.ID_Pasien
join Obat o on o.ID_Obat=r.ID_Obat
where p.ID_Pasien='P0005'

insert into Invoice(ID_Invoice, Tanggal_Invoice,ID_Pasien,Biaya_Perawatan,Total_Pembayaran)
values ('IV006','20230524','P0006',20000,20000)
update Invoice set Total_Pembayaran = Total_Pembayaran+o.Harga*r.Quantity
from Pasien p
join Resep r on r.ID_Pasien=p.ID_Pasien
join Obat o on o.ID_Obat=r.ID_Obat
where p.ID_Pasien='P0006'

insert into Invoice(ID_Invoice, Tanggal_Invoice,ID_Pasien,Biaya_Perawatan,Total_Pembayaran)
values ('IV007','20230602','P0007',50000,50000)
update Invoice set Total_Pembayaran = Total_Pembayaran+o.Harga*r.Quantity
from Pasien p
join Resep r on r.ID_Pasien=p.ID_Pasien
join Obat o on o.ID_Obat=r.ID_Obat
where p.ID_Pasien='P0007'

insert into Invoice(ID_Invoice, Tanggal_Invoice,ID_Pasien,Biaya_Perawatan,Total_Pembayaran)
values ('IV008','20240101','P0008',150000,150000)
update Invoice set Total_Pembayaran = Total_Pembayaran+o.Harga*r.Quantity
from Pasien p
join Resep r on r.ID_Pasien=p.ID_Pasien
join Obat o on o.ID_Obat=r.ID_Obat
where p.ID_Pasien='P0008'

insert into Invoice(ID_Invoice, Tanggal_Invoice,ID_Pasien,Biaya_Perawatan,Total_Pembayaran)
values ('IV009','20240104','P0009',250000,250000)
update Invoice set Total_Pembayaran = Total_Pembayaran+o.Harga*r.Quantity
from Pasien p
join Resep r on r.ID_Pasien=p.ID_Pasien
join Obat o on o.ID_Obat=r.ID_Obat
where p.ID_Pasien='P0009'

insert into Invoice(ID_Invoice, Tanggal_Invoice,ID_Pasien,Biaya_Perawatan,Total_Pembayaran)
values ('IV010','20240104','P0010',20000,20000)
update Invoice set Total_Pembayaran = Total_Pembayaran+total
from (
select sum(o.Harga*r.Quantity) as total
from Pasien p
join Resep r on r.ID_Pasien=p.ID_Pasien
join Obat o on o.ID_Obat=r.ID_Obat
where p.ID_Pasien='P0010'
)qty

select*from Invoice
select*from Obat


DDL Perbaiki Masalah Dibawah Ini
1. tabel invoice masih belum mampu menginput total harga obat yang dibeli lebih dari 1 jenis
2. nik pasien ada yang tidak sesuai dengan referensi format ttl
3. jumlah pembelian tabel resep belum mempengaruhi stok di tabel obat
4. tambahkan kolom gender pada tabel pasien dan tambahkan panggilan mr atau ms pada nama pasien berdasarkan gender
5. buat prosedur jika jumlah obat yang dibeli melebihi stok maka transaksi tidak bisa dilakukan

DML
1. Tampilkan Semua Nama Lengkap Pasien yang pernah berobat di klinik
2. Tamppilkan nama lengkap dokter beserta str nya dan yang termasuk dokter umum
3. Tampilkan obat yang paling banyak digunakan berserta harganya
4. Tampilkan medical record pasien yang ditangani oleh dokter Sofiya
5. Tampilkan harga obat yang paling murah dan paling mahal pada klinik
6. Tampilkan nama obat beserta stocknya yang memiliki tanggal exp yang sama
7. Tampilkan jumlah obat terjual tiap bulan per periode tahun
8. Tampilkan jumlah pemasukkan klinik perquartal pada tahun 2023 (Q1-Q4)
9. Tampilkan total transaksi per pasien per tahun
10. Tampilkan total transaksi pasien yang terendah dan tertinggi per bulan per periode Tahun