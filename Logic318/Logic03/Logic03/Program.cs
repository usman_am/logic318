﻿
//break_dan_continue();
//convertArray_stringtochar(); 
//findIndexOf();
//datetimeargs();
//datetimeproperties();
timespaninterval();
Console.ReadKey();

static void timespaninterval()
{
    DateTime dateTime = new DateTime(1997, 11, 15, 00, 09, 00);
    DateTime dateTime2 = new DateTime(1997, 12, 24, 10, 30, 00);

    TimeSpan interval = dateTime2 - dateTime;

    Console.WriteLine(interval.ToString());
    Console.WriteLine(interval.Days.ToString());
    Console.WriteLine(interval.Hours.ToString());
    Console.WriteLine(interval.TotalHours.ToString());
    Console.WriteLine(interval.Ticks);
}

static void datetimeproperties()
{
    DateTime dateTime = new DateTime(1997, 11, 15, 10, 09, 30);
    int tahun = dateTime.Year;
    int bulan = dateTime.Month;
    int hari = dateTime.Day;
    int jam = dateTime.Hour;
    int menit = dateTime.Minute;
    int detik = dateTime.Second;
    int weekday = (int)dateTime.DayOfWeek;
    int weekend = (int)DateTime.Now.DayOfWeek;
    String hariini = DateTime.Now.ToString("dddd");
    String harinya = dateTime.ToString("dddd");

    Console.WriteLine($"Tahun {tahun} bulan {bulan} hari {hari}, jam {jam} menit {menit} detik {detik}, weekend di {weekday}");
    Console.WriteLine("weekday minggu ini {0}, hari ini {1}, harinya {2}", weekend, hariini, harinya);
}

static void datetimeargs()
{
    DateTime dateTime = new DateTime();     //default datetime
    Console.WriteLine(dateTime);

    DateTime dateTime1 = DateTime.Now;      //other default datetime
    Console.WriteLine(dateTime1);

    DateTime dateTime2 = new DateTime(2010,02,12);  //datetime with spesific
    Console.WriteLine(dateTime2);

    DateTime dateTime3 = new DateTime(1997, 11, 15, 10, 09, 00);  //other datetime with spesific
    Console.WriteLine(dateTime3);

    String dt = "1997/09/09";
    DateTime dateTime4 = DateTime.ParseExact(dt, "yyyy/MM/dd",null); //parse detetime
    Console.WriteLine(dateTime4);

}
static void findIndexOf()
{
    List<int> list = new List<int>() {0,1,2,3,4,5,6,7,8,9};
    int[] arrays = { 1, 2, 3, 4, 5, 6, 7, 8, 0};
    int patokan = 8;
    int patokan2 = 1;

    int index = list.IndexOf(patokan);
    int idx = Array.IndexOf(arrays, patokan2);

    if(idx == -1)
    {
        Console.WriteLine($"patokan {patokan2} tidak ditemukan");
    }
    else { Console.WriteLine($"patokan {patokan2} ditemukan pada index ke {idx}"); }
    if (index == -1)
    {
        Console.WriteLine($"patokan {patokan} tidak ditemukan");
    }
    else { Console.WriteLine($"patokan {patokan} ditemukan pada index ke {index}"); }
}

static void convertArray_stringtochar()
{
    String kalimat = "lebak bulus kopi hangat sekali";
    char[] ArrayChar = kalimat.ToCharArray();

    foreach (char c in ArrayChar)
    {
        Console.WriteLine(c);
    }
}

static void break_dan_continue()
{
    for (int i = 0; i < 10; i++)
    {
        if(i == 3)
        {
            continue;
        }
        Console.WriteLine("angka ke {0}", i);
        if(i == 8)
        {
            break;
        }       
    }
}